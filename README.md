# euShipments SDK

Client library written on PHP for euShipments (by InOut).

## euShipments API Documentation

https://documenter.getpostman.com/view/26992907/2s93Y2S2Q8#intro

## Getting Started

*    Composer is the recommended way to install the SDK.

*    If Composer is not already in your project, download and install Composer (http://getcomposer.org/download)

        - For Windows, download and run the Composer-Setup.exe (https://getcomposer.org/Composer-Setup.exe)
        - For Linux, follow the Command-line installation on the Download Composer page (http://getcomposer.org/download)

*    Then simply run the following command:

        composer require inouttrade/eushipments-sdk

*    Once you do this, composer will edit your composer.json file and download the SDK and put it in the /vendor/ directory of your project.

*    Then Make sure to include the Composer autoloader at the top of your script.

        require_once '/path/to/vendor/autoload.php';

## Prerequisites

In order to use the API, you must have a valid authentication token. If you don't have one already, write to support@eushipments.com.

## Examples

Before running the examples, replace the value of `AUTH_TOKEN` with your authentication token and `COMPANY_ID` with your company ID (see `01. Get all companies associated with the account`). Also check the values of any data that are sent in the request.

### 01. Get all companies associated with the account

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CompaniesRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CompaniesRequest();
    $request->setTestMode(true);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 02. Get all couriers enabled for the account

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CouriersRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CouriersRequest();
    $request->setCompanyId(COMPANY_ID);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 03. Get all countries enabled for the account

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CountriesRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CountriesRequest();
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 04. Get all counties for country

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CountiesRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CountiesRequest();
    $request->setCountryId(1);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 05. Get all cities in country

Without using pagination:

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CitiesRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CitiesRequest();
    $request->setCountryId(1)->setPaging(false);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

Using pagination with 10 results per request:

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CitiesRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CitiesRequest();
    $request->setCountryId(1)->setPaging(true)->setFirst(10)->setSkip(0);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 06. Search for city

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CitySuggestionsRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CitySuggestionsRequest();
    $request->setTestMode(true)->setCountryId(1)->setKeyword('Brasov')->setSearchAllFields(true); // You can use city postcode (500002), name (Brasov) or another keyword
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 07. Get city by ZIP code or name

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CityRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CityRequest();
    $request->setCountryId(2)->setZipCode('4000')->setCityName('Plovdiv');
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 08. Get all courier offices

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CourierOfficesRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CourierOfficesRequest();
    $request->setCourierId(328);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 9. Get all courier offices in city

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\OfficeAddressesRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new OfficeAddressesRequest();
    $request->setCityId(31744);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 10. Creating a product in the fulfilment system

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\Product;
use BogdanKovachev\Eushipments\Request\CreateProductRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $product = new Product();
    $product->setName('Test Product')
        ->setBarcode('0000000000000')
        ->setBarcodeType('EAN-13')
        ->setDescription('Our new bestseller')
        ->setLength(40)
        ->setWidth(25)
        ->setHeight(15)
        ->setWeight(2.5)
        ->setReferenceNumber('TEST-PRODUCT');

    $request = new CreateProductRequest();
    $request->setTestMode(true)->setCompanyId(COMPANY_ID)->setProduct($product);
    $productId = $request->makeRequest($eushipments);

    // The ID of the newly created product in the euShipments platform
    var_dump($productId);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 11. Get all products in the fulfilment system

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\GetProductsRequst;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new GetProductsRequst();
    $request->setCompanyId(COMPANY_ID);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 12. Creating a request in the fulfilment system

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CreateRequestRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CreateRequestRequest();
    $request->setTestMode(true)
        ->setCompanyId(COMPANY_ID)
        ->setWarehouseId($request::WAREHOUSE_RUSE)
        ->setProducts([
            [
                'refNumber' => 'TEST-PRODUCT',
                'quantity' => 50
            ],
            [
                'refNumber' => 'ANOTHER-PRODUCT',
                'quantity' => 25
            ]
        ]);
    $requestId = $request->makeRequest($eushipments);

    // The ID of the newly created fulfillment request in the euShipments platform
    var_dump($requestId);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 13. Creating a order in the fulfilment system

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Datastructure\Airwaybill;
use BogdanKovachev\Eushipments\Datastructure\RecipientFulfillment;
use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CreateOrderRequest;
use BogdanKovachev\Eushipments\Service;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $recipient = new RecipientFulfillment();
    $recipient->setName('Test API')
        ->setCityId(31744)
        ->setCityName('Пловдив')
        ->setZipCode('4000')
        ->setRegion('Пловдив')
        // ->setOfficeId(329)
        // ->setOfficeCode()
        ->setStreetName('бул. България 1')
        // ->setBuildingNumber(1)
        // ->setAddressText('ет. 1 офис. 2')
        ->setContactPerson('Test API')
        ->setPhoneNumber('0888888888')
        ->setEmail('email@example.com')
        ->setCountryIsoCode('BG');

    $awb = new Airwaybill();
    $awb->setParcels(1)
        ->setEnvelopes(0)
        ->setTotalWeight(2.5)
        ->setDeclaredValue(100)
        ->setBankRepayment(100)
        ->setOtherRepayment(0)
        ->setObservations('Additional info')
        ->setOpenPackage(false)
        ->setSaturdayDelivery(true)
        ->setReferenceNumber('REF-1')
        ->setProducts('Test 1 Test 2')
        ->setFragile(false)
        ->setProductsInfo('Products info')
        ->setPiecesInPack(2);

    $customsData = [
        'dutyPaymentInfo' => 'DDP',
        'customsValue' => 100.0
    ];

    $request = new CreateOrderRequest();
    $request->setTestMode(true)
        ->setSenderId(COMPANY_ID)
        ->setCourierId(15)
        ->setWaybillAvailableDate('2022-10-15')
        ->setServiceName(Service::CROSSBORDER)
        ->setRecipient($recipient)
        ->setAwb($awb)
        ->setProducts([
            [
                'refNumber' => 'TEST-PRODUCT',
                'qty' => 2
            ],
            [
                'refNumber' => 'ANOTHER-PRODUCT',
                'qty' => 1
            ]
        ])
        ->setCustomsData($customsData);
    $orderId = $request->makeRequest($eushipments);

    // The ID of the newly created order in the euShipments platform
    var_dump($orderId);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 14. Attach PDF files to order

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\AttachPdfFilesRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new AttachPdfFilesRequest();
    $request->setTestMode(true)
        ->setOrderId(1)
        ->setReferenceNumber('REF-1')
        ->setFiles([
            [
                'base64Attached' => 'JVBERi0xLjQKJdPr6eEKMSAwIG9iago8PC9UaXRsZSA8RkVGRjA0MUQwNDM1MDQzRTA0MzcwNDMwMDQzMzA0M0IwNDMwMDQzMjA0MzUwNDNEMDAyMDA0MzQwNDNFMDQzQTA0NDMwNDNDMDQzNTA0M0QwNDQyPgovUHJvZHVjZXIgKFNraWEvUERGIG0xMDMgR29vZ2xlIERvY3MgUmVuZGVyZXIpPj4KZW5kb2JqCjMgMCBvYmoKPDwvY2EgMQovQk0gL05vcm1hbD4+CmVuZG9iago1IDAgb2JqCjw8L0ZpbHRlciAvRmxhdGVEZWNvZGUKL0xlbmd0aCAxNzM+PiBzdHJlYW0KeJx1j00KAjEMhfc5RS5gJ0nTSQviQhhnrfQG6giCC8f7g+38LQSbkIR8vEfKSCV2XEpUwesL3uAsTNu1lyVjjUuP8zA+oOk9Pj5QuSVFZvE43mGA84+DSc3isSiOGZpTEahr6zPMA/B6hVhybeQY0KmlyOYT5hdU5p2YefUR8w33RF4OmJ/A7JSIiaw4zCTMJLoUpBUOG9BuARyYyeIG5J+CdAJdLt/6AgaoPNcKZW5kc3RyZWFtCmVuZG9iagoyIDAgb2JqCjw8L1R5cGUgL1BhZ2UKL1Jlc291cmNlcyA8PC9Qcm9jU2V0IFsvUERGIC9UZXh0IC9JbWFnZUIgL0ltYWdlQyAvSW1hZ2VJXQovRXh0R1N0YXRlIDw8L0czIDMgMCBSPj4KL0ZvbnQgPDwvRjQgNCAwIFI+Pj4+Ci9NZWRpYUJveCBbMCAwIDU5NiA4NDJdCi9Db250ZW50cyA1IDAgUgovU3RydWN0UGFyZW50cyAwCi9QYXJlbnQgNiAwIFI+PgplbmRvYmoKNiAwIG9iago8PC9UeXBlIC9QYWdlcwovQ291bnQgMQovS2lkcyBbMiAwIFJdPj4KZW5kb2JqCjcgMCBvYmoKPDwvVHlwZSAvQ2F0YWxvZwovUGFnZXMgNiAwIFI+PgplbmRvYmoKOCAwIG9iago8PC9MZW5ndGgxIDE3NDA0Ci9GaWx0ZXIgL0ZsYXRlRGVjb2RlCi9MZW5ndGggODQ2OT4+IHN0cmVhbQp4nO17eXwUVbb/ubeql3S2zkoWQlVokmCaSAiBsETSWUEjEAhiB0UTIAjKEiABRQVcUAioqKiIW8QBGUGpdBjssAxRxhnXAVxxGzIDKjrwABd0ULp+33vTICAPZvm9P977WMX5nruc773nLnWqbkKIEVEMQKXswSWlZczBkGEHUZo7uGJ4JcWRA/l85MMGV15R5Pi9bSnyW5HPHl7ZM+cmjz+WiK9Hvnp0yVDvaEvdfCJ9OVHU8vFTa+rYYPYG6r2oHzV+dr3+46BnPiJyHiGyjZlYd91U+4HN3YlCE4gsH1xXM6uOoigE7Q+BvfO6KTdNnH1kN/q2gq+2TZow9cZFx0LqiBIuJ7IPmVRbM6H9mvenon0d9n0noSB6Xkhn8Fcg323S1PobUzoro4mUjSi7fMr08TWp73Xrg3wY6tdOrbmxzp6vzkad8E+fVjO1NmFJ7znwBVk2um76rHozkz5A+kZRXzeztu7oZZ/FE+XA56i3ScydQ97iUshOHGlmmkiLukr6mvLpPrKi3Ek96QqMOp+/jOY5LORlZoj2z3GBbxsUGEbFTjp+/Phcpyw546oM9n8xbkvNzJpxpI+/aeYU0q+bWXsD6ZNqx80kfUpN/TTSf26TwoJpDq86ylTcjLriZpSDm9Ew3IxG4maSY4m9uXtyif/ayPzv7Il2SVu1Lz9F6DcqBm46fvynE06yd4NtyGlecjknRLHB2YhFPx7pgZgnlVKpgIbTDTQd80Wn58x9J++zxqwo7/KtmD27ZaWlN1xP7tDKbprIo+0WHmpTubhUOmu2hg4fNhyzkErTLe8ERrDetkHM5xELhZ7VdMtmsbqknka48j+4/3z+m438N+8DHTevtp41Lfw58p9zD/0PX+o+uv4/4Vv7U9X/L1/+mUudRYvPyO/DDsdjRmPEDlURdzCeB4JphidrUTDNsWMXBNMKDaCyYFrFns4Jpi2URAnBtBUpokKaSZOphqZQFhXRdOgJNJRG0WiqRc0s1E0n8Wz2wfObTf1g+TNDP40hrKZTPd1EdWDqdClNhc11sJwG1NG6fk7mSf1z3W9Rm4O+euHW4ckk2d4veyhGbibSAmtQ3uHpxbKfKbKPkSi7Dvx6jEPkaqHFqGYDJ8BSPP3Yk+wAni0LosLNm3lvCuU5HrfD4knUciMtmoVbxtj7WRVOIVbHfaEsNLFTkhKSbrWn29R0pqRz62a+nGx8uSeM6yyb3ccUlugI9TN7S+rn6xLc7mHfjs0fun+/81DHPcxZWlvy+VgqyC/IH+o88flYd69sVlZSVsKUqNQoRQBjl7LsIZ+wRDaXH2DewNoTCYG7WGLgC3iLnazeZXmNkkmjOzyZVmdETK7F6YzOHZAwINFjuSJuYtI6mzUkPoak/7DiNKZLv86Rfv6gz7kyzM/rPV1jWGdHZ6YRS8frKyTVGa/H8/ik1MhUp5M5E/XVxQnuYc5vxw6Fs8eA0f17HnLmO/OpoODEt/ulz9H9+/fKprFuN3Ol98nt2zevb5/cdFdXqy2tb7feOWpcrNVmtYqx8MMmG3ksIe2O6TcsSw4EQlny51+zLpPXV7lPiPHlhd3S9KrWa+DIhrm36i3HT6wdu+a+y8YEooOjrTK/sHxheYciMd5VnisesTxiXxG2IkK1M1uEPdKWkJFwY8icaNucqBvj7lIX2xeH3RWxMHpx7KK4RZ0WJdyVFGaLtsfakuKik2KTEuKSbDFZ4SGJWTYlPmODAw+O06E7FIefL/Xo2SmelOqUupQFKU0pVj3lSApPcWY0ERMTmI3HBUYtneft6JiXGUMPDXWOnXFMJKjgUMEhrODYsTNobExunpiK3jpFOSlVJxYb3TsnODNVxTnPX7e4hZWwhYF5gW2B1sA81uvz5uZ9n774Yjt/t31Fnc89IDAtsDLwRGA6dtGkfwQQ93/64UfxjkA8UK/FHg2lb1pJMT9tCY8qUPxmm+fWxKxcm+JUYqwZIROtGxzbHa+GvOH4yOGoVKoVHm5LCCmzXmmfbbVsCtmrHlJ/Ur+zWobZhtknWm9Vl6qPqY9bVlpX2lbaHZoabXWrbkumNdOWae8ZXq6WWxxWqy3EEWJ3WBwhilUNtahWjpAeGmq3Yd4coaqfT/UkWXra+2s2ZqsN56HpbAGJfUWJYQU3Bycrf+iJ/ETnsRkJh74N7qJ8oNj+0Z36322/1fkHe36zlReP8oqhveoLSc0ld5WcUszpTOyx3uKZSGX4Z4tajF1zKRsTeAjTuDvw3R2WzT8dY7MDt5y4hn26OLAee2akeUBdqQ6icEqkRzxDDrAv7N/HfB+n/okfsPDoREtiCK9yjo4ZHV+V8AhfYV1hfyTMH/Ie/9jySch7Ydhu1gPhzmftb/A3rTvsfwyzNNgXW++0K1F4cHyO0E7i+YlVbbH9bUnVyXXJPDkilRKTvIVysOKBmXHs1KagGWPZ2BnFXk/IZOfE6InxkxNUNraKUBiTG923dw7FxZKra7f0tNj4U9tkZOOJx4+y3MBrBx8IfN/I9IenTXvooWnTHuZdlzJrY+BPh48Gdtxprn1y7dqmx9euJU/hKE/BoEvyBw7o3y+vT27vnF7ZPS/O6uHOvKh7RnpaN1fXVF3rktI5OSkxoVN8XGxMdJQzMiI8LBSrarNaVAXr2aPUVVatG+nVhpruGjIkS+RdNSioOa2g2tBRVHamjaFXSzP9TEsPLCeeZenpsPScsmROPZ/ys3ropS7deKvEpfvZmBFepO8pcVXpxiGZHirTy2Q6HOnUVBD00oRJJbrBqvVSo2z2pMbS6hI01xzqKHYV1zqyelCzIxTJUKSMTq66ZtZpEJMJ3ql0QDPekeFwykhylZQaia4S4YGhpJXWTDAqRnhLS5JTU6uyehiseLxrnEGuIiPSLU2oWHZjWIsNm+xGnyxGQ0v05h5tjUv9ThpX7Q6b4JpQc7XXUGqqRB9RbvRbYnSauz/h5ywajy723n16bbLSWJowWRfZxsa7daNthPf02lSBVVVoA1yeVlbdWIaul2ISyyt19MYXVnkNthBd6mIkYlQd46t1lYqS6ut1I8RV5JrUeH01liap0aCRN6X6kpI8rWY7JZXqjaO8rlSjINlVVVPSuTmWGkfe1JLo0RPPrMnq0eyM6pjY5ojIYCIs/PRE7ak6mZLmIlU+8tTMMuGR61JsCEMfr8MTrwtj6iegth81ju8HM1xVDCxjAlZkshFSXN3oHCDKBd+wpDldeuN3hB3gOnTwzJKaYIk1zfkdiaTYJ6e2GupPpg2328jMFFvEVow1hY+DZL5PVo/Zft7XVefUoTB9VIG5raka0BPTn5oqFniJ30PjkDEWjPB25HUal+wjT093lcGrRU3byZq4K0TNgpM1p+jVLuzkjfL7OM6wp5/6F+mMjymdNMBg8eepru2oL690lY8Y49VLG6uDc1s+6oxcR32/U3XBlBFT7FWSeTDFkxVZi0159SljkfGGGWoa/lnlpp5gKNiUsoDpZYazekgHVjlSU/9bjt9mP43kN48IllQ/04JeGgPcZ+YHnpE/w7uwRgX+qum8fNSYxkbHGXVlCECNjWUuvayxurHGby4Y59KdrsZW/ix/trGutPrkgvrNzUuSjbKlVRjEJDYAm5VTUbOLLRrR7GGLKsd4W3GS1ReN8vo448XVRVXN3VDnbdWJPLKUnyoVOV3kqJxho/u4XVYlt3qIFshaVRbI/Hg/I1lmP1nGaLyfd5Q5ZRmuLMK7vrOUZ6mzmk6dcQLff1ICk839ok5o/hU+D1I6JHj5aD19wLoznVrYcepEP+CN2Qtf4yp9jw+ZDXSCHsJ5YBQ9zKKpG8XjtH8pU2HjpqXsMXO2+SVdgtPFKvNFdrv5HOrvoz/SD/DgLyqjPJy4r8BdS18qn+HrbCVOHHfjq2QgjWTx+AZ/H/d38OFBWk6/Z7eYP8izx+1oLx/f5IXmS+ZPlElL1WWWPSG/o/tpC7Oa483J1AWn+kbuNt8391I6TlvP0Hr45GZt6hCchm+ghbSCJSp/ROoh+g0FWBgfqxRbtqOnS3FOmUZzqJGeo9dZNKuw7LEcMW82v8DpJoa6w6fJ9CXrw4by1WqYOcj8iK6iVnoV4xV3m3qV+qzlqkCB+YT5MsXRi8zBtrKXLDmWe0/cZj5tvkBh8KcXZmQY+hlHd9BL9Bodpa/5fHM+DaFK9PwKS2E6S8eMv88T+Tw+T3kHJ4tCGgtvG+gpMrAim2kLbcPcfEzt9BmLZcnsMjaO3c++5mF8At+pPKZsVN5VmfpbzLeL0jBH9bSaNtGb9BbtZBYmDhYV7Ho2nT3CnmDt3OAH+feqXb1D/VE9YUkPtAd+NIeZ3+E8l0SX01yaj7l9hlpoI07y79HX9A0dY07Wj01iTzODtbODPIR35cN5HX+Yr+bPK8OU+5WX1D5qkXqD+pb6keUuyxJbjS3w05rAg4HnA7vNF83d2DsRaD8dp8rJdBt2xWraTu+g9Q/pU/qb2D9ofyAbw65BL7PYIracPc9eYbvZVxglybsrH8hL0Ot0PhPzdDt/kC9H7ztx7+If8U/53/l3ikXpqvRVZihPK4biV3Ypn6tONV29WO2lDlfHqCZWJscy2FJpWWtZZ3nZcsSab51grbMesN1uu9P+5onME38JUGBSwAi0YO/asZPmYiaepFXY9xuxBq9jRv8Mj9vpW6xCEr4oM+B3f1bGytlQdiW7mtWy29nd7AG2gj3GVrEXMAKMgdvgu5sX8kpew2v5nfxufg/fiHszf42/z/fwQ/C8k+JS3Eov5VJljHKVMg1jqFfmKXdiZu9XnlN2Ku8oXygHlENYtU5qF7VBnas+qj6rblR3Wy63TMW9yrLd0mbZbfnJ8pOVW5Osna09rddb11r/ZrPa+toqbItt79q+sdexziwTnuun/4SAJ+IZ7MKf47HqfHYIBSlMxZnpfnJjHSrxVHxDBUoA6xIh6uFbHE9UYwTT6lEN8OvZFurDXqH5Vq6IH7K2k499wtvVHfwSeg9v0ET1WWWa5XWeSusQjZbxrXwLK6KNPJ+P5o8rxD5ja+kz7PcbaTm7gc2idewQG8BuZXlsPr3L45VKdiflm6u4ykLw9X6E4AHdpk6ga87/kxDWnz6hLwNPquHqLYhPfnoYK7qe9rLf0nFmMQ8iuimIRjWIMkux3xeSiHpj8ZzNx/OYiAgyxbqTNjIrkS3POkidS0foH/SlZTN2VBEi6ReByeqT6j4zz8zCE4anjNbiuZtEg/HEfIZdsg15kbsaT7oDsSQHT3UFjaEJdCui3v2mYT5u3mHeZE6nN8A9znqw46wJT4QfjHx6Ffd99CFbgudw8L/2E6CTV2ACtdFXLIGlsRw8D4cssy3LLM9ZNlp+b3nL2guzfSc9hh39N+xmB0YwnnbTV/Q9s2NtEqkH5cLffvDdS1N4lbKNilkS1eGZ7Y44XhQcySy0cjtm73E8z9vwbBxBnLiafk97GGedMKLx6N+Odsoxz9fCeg1W8A7WgpIJiNqZ9HeMO4L14/Xoz4OWHkbUaoNPn9DnmG1T+tUDcaGEjUZb39OVNAE99KUK1owV2ET9EVlLlDcx392Yk4pYV/Yb8KrxhEZQCvW37GOcegSGmf34ZGUb3jEmypvw9kqmS9gMeBGJcZygODac+gRGwod3mKIa7G3pxaO81rxbmROYQm/Qb7EmHnW2rUROrPi5lzfULn5GrJx3BWxn5e0XsBUSFhLyL7cb8k+0G+4QP8BXz2P5S/8cF2hX2EeGhV6w3bP9Czun1c8+CHtnWNgF2z3bv/O1GxK0jwoPB1r/pXbD/4l2YyIjqON3Kv/9dbZ/keexDQ3axznxLfmLFT+/f84L+CDsE6KjL9ju2f5FX8AHYZ8cF0vn3+lYg7PycRfwQdinxMdfsN2z/Ys/j60zaK8niMf5fDtd/FLnzCvhAj6Il2NqYiJ1rOI/327iBdoV9mkpnS/Ybqez8inntOq44oL2mak6MOK87SadlU+9gA/C/uJurgu22/msfLcL+CDsczLS6fxPEJF2Vj7jAj4I+76ZF9Evd+iZ19njzjyPbZegPU6JdP4niMh1Vj7rPLZ60L44N4fO/wQRXXRWPvc8tmlB+/IB/ej8O53kr0BOvwacxzYzaF9ZOIh+uZPOvHqflS+8gA/C/uohpXT+nU7U76z8kAv4IOwnDCsnOvXL5XNfl5yVH3YBH4L2CilMXBZFwfcSowTLwdA2+sFuIszZzQBeLSFABzmAoRRqnkDwDgOGUzgwQmIkReBU7KRIYJTEaIoCxlA0MJZizB+xOQTGUxywE8UDE6iTeRzhJgGYJDGZEs1/4CFIAqZQMrALdQZqlALUqQswlTRgV9JxMncBv8eD2hWYRi5gOnUDZkjsTmnAiygdmEkZ5jFyU3ecMHtIzKJM4MXkBvakHsBsygL2kphDPc1vMf3ZwFzqBewD/Ib6Ug4wj3oD+1EusD/1AQ4Afk0DKc8U/w+hH/AS6g8cBDxKBTQA6MH54Si20SXmEXy7DgIWSyyhAmApeYBlVAgcLHEIFZuH6VIqMf+LLqNSYDmVAS+XOJQGA4fRpcDhdBmwgsqBI4AHaSRdbh7C2X4ocBQNA14hcTRVAK+kEUAvjYRlFVUCx0i8ikYBr6bR5t9xBrkSeI3Ea8kLrKYq8yucVMYAx9FVwPESJ9BYYC1dA5xI15pf0nUSJ1G1eUD8PhR4PY0H3kATgFMkTqVa4DSaCJxO15lf4Lt+EnAGTQbOpOvNz/GtfgOwnqYAGyTOpqnAOTTN/Axf/XXAm2gGcK7Em2km8BaaZe7H+aYeOE/ifJpt7qMFNAd4G90IvJ1uAt4h8U6aC1xIN5t/o7voVuDdwL/SIpoHXEzzgY20ALiEbgMulXgP3QG8l+4023FGWghcRncB75f4AN1t7qUHaRFwOTUCHwL+BWeLJcBHaClKVtA9wEfpXuBKiY/RMuDjOJftpSfoAfNTnPsFPkUPAptoOfBpehi4ih5BO89I/A2tQMlqehS4hlYCnwV+gpPf4+bHODU8gfRz9CRwHT0FXA/8mJ6nJuAL9DRwAz0DNOg3wGaJPlptfoRT3xrgRnrW/JB+J3ET/Rb4Ij0H9NM6YCutB24G7qEt9DxwK70A3EaG+QFOYQK3UzOwjXzAl6gF+DJtBO4Avk9/oE3AV+hF4B/JD/yTxFep1XyPXqPNwNdpC/AN2ma+i5OZwLfo98A/03bgTmoD7qKXgLvpZfMdept2AN+hP5hv07v0CvA9iegB+AH9CbiHXgN+SK8DPwLupo/pDeAn9CbwU3rL3EV/kbiXdgLbaRfwr7Qb+Dd629xJ+yTup3eAn9G7wM/pfeAXEg/QB+af6UvaA/yKPjTfwknzI+BB+hh4iD4B/hd9CjxMfwEeob3Ao8A3cXpvB35DfzXfoG9pH/A7icdoP/B7+gz4A30O/Ad9Yb5Ox+kA8Ef6EvgTfQU8QX8HBoCv4cR5EPhrTD9XTP9WxvRvZUz/9hcx/RsZ07/5RUz/Wsb0r2VM/1rG9KMyph+VMf2ojOlHZUw/+ouYfkTG9MMyph+WMf2wjOmHZUw/LGP6YRnTD8uYfljG9EO/xvR/K6bv+49j+l9lTP+rjOntMqa3y5jeLmP6XhnT9/4a0/+NmL71f3FMf+vXmP4/GtOPyZh+TMb0YzKmH5Mx/ZiM6cd+jen/52L6vl9j+q8x/deY3jxqQWG4sp42KOK/AjqBOqQJopBHWd9iC8/x+KGjY6X2xbtzWs02JAb0luVZy3MWbFXWIRj0RvE63xWieF2LpyRH6t4DO3TPXlL77B3VttgcrTAJtJ4QTpHB1HDIfZCnINshVji0jvZCTIiirFVW+co0tLAaDUUWxiqricHL1bQTYkIUeL8aY1lNh4MlKrx6piUkTHT/jGQlK8+AFQl0QhZANkB2Qiw0HfgUxIQoSK1C3SriyirlaZ9TcxY6lCdpPoQrKymSMbx02pQVLU45N4+2RMbkeAqdykNUAeFkKEOpDcLR7P2g3U8c5uW+rF5yCstbHBE5TtgvgdNL4MgSdNkEZDLvgQj7JS0x8aL5O3yRUZJ3sy87tyPR4kzIqcAs3EhMqVWm4cWnKfOgu0CPh06BHqdMwEtZ+OlpiXTmLEB/BTAvUOLwDtSUQiUebzVNKVGSxP+QhlmDL6KjnwZf98wcjLhYSZAmkUo4XnqaYldsvhxN36J45OQvagkJFf4t8jnjcrYpCxUbXu2asgBWnbTIbYoDK+uQIxnVEhKes6wwTBmFYY7CtGjwkWGWp8mGpvnQUGGUUqp0xgeBptygpODzQFPKlC5SP6s8jRehpjzRkt5Za9uiPChZD4hG0f2gjq01qCU8IqetMEQZhFpDuRcLcK/sfFlLer8cKkxXulM2hGOO5yM1X276RqQasWqNWKlGrFQjnGoUvzBTFqNmMWx6KnOpTplDyyBPIS22VZwPE9oqE92657QqiUoCJsa5BVPJUJrUEhIhPEvwRcdIs4SWsIicgm3KLOzzWWjTo9S3dErImb5FyZRD6dGSkCwIdT5s121Kp46lATFeLMk2pTMmQkxMitLFF6cZhRryYiNrxPjrfJeYJP4Of08st/gfIlK/EdRvBfWfO7TZxnd1PBT8baHbCzvzz9DYtRyvFaQ438J34NNH4x9xv/CCf8hb8cGi8T3IT4Buhe4NvdmX+qrm5/4WKPj+mC88XgyW7/C5ewYTWlow0Sk5mIiOzylM4y/zl/BVp/EPoLtBv8Tb8AWn8e3QCdBtvB7RXuO/433wDaXxjUH9B75VbHH+It+Ery2Nt/gihAuGzybUBp9VqBd81JGr6Klt5S/wdfiY1PjzvvQklK5tSe+mRW5Be4yv5vW+FC260MGfZl72LYyaaI/QFM1X+fJEI8t8W3WtlS/jyzwJeZ40T5ZnjZKdlp2VvUbR0/QsPU9foxc6+b0IIE9xPL98CTCPdI7dA/FAlvHFPjXPKDyBMYlxcVoAbJKpamCdTBHQear2iEwV8IU0HMLRxjzIfMgCyG2kAudCbobcArlVltRDGiBzEE3qwKgDow6MOsmoA6MOjDow6iSjTvbeABGMajCqwagGo1oyqsGoBqMajGrJEP5Wg1EtGRVgVIBRAUaFZFSAUQFGBRgVklEBRgUYFZLhAcMDhgcMj2R4wPCA4QHDIxkeMDxgeCQjG4xsMLLByJaMbDCywcgGI1syssHIBiNbMnQwdDB0MHTJ0MHQwdDB0CVDB0MHQ5cMJxhOMJxgOCXDCYYTDCcYTslwyvVpgAhGOxjtYLSD0S4Z7WC0g9EORrtktIPRDkY7n9Os7Cp8BZRdoOwCZZek7AJlFyi7QNklKbtA2QXKruDQ6+VkcGybeZD5kAUQwW0Dtw3cNnDbJLdNbq8GiOAaYBhgGGAYkmGAYYBhgGFIhgGGAYYhGU1gNIHRBEaTZDSB0QRGExhNktEkN24DRDD+9U35Ly8Nv4157XjX8gXsIqnn00Gp59EeqW+lZqlvoTVS30y3Sz2X8qSeQ+lSoz2p60mzM5+WF1kYjxAwHHItZDrkKcgGyHaITaZ2QvZCTN7H01WNtA23PWXbYNtus2ywtdt4pHW49SnrBut2q2WDtd3K9cJkHi7jKEIL3SdxPvAwBC8RYIFMFfBc9JuLONsHdy7P9UQd0g9nsp2ZbHsm25DJ7stkhSF8MFNlpNMpj8Nx5vWEpQ/S9kDy0jMGITLdu+lgJ82X3lfzs60d6iKPG/ogpBmyBnI7JA+SA8mCpEE0WZYJe6+na7DJrZAMSCpEF12Q/O8H0VF2TysPZ2taXgmnENFPRnfwtvgysqH8vozhUC/6MsZphSFsE2WIryL2O6zcOugNPm0/qp/vUOt92haotT4tF2qsL+NiqKt8GW9pheHsCtJUQR0V1JUYt9AjfdpomI3waRdBuX0Z6cIaJ3eWhtqLmBdf2ppIS1a3jp5cPm0gVFef1l9Y2ylDLDyzUpZ0zwIRWmmBQ4dbmVdlnlDtkPagdhD0v2NisT0+1P0q1M40PxvtcWhbs56EcaHmK3QIe7wfmoPaEPp32pq0xdpjaIulbdIe1S7W7s3y21F8D/xeLLvwabfrfr7OE6Mt0LK1+qz92iztMq1GG6mNTUO5T7ta2yrcpCrm5es2aRVo8FKMIs2nDU7zSxfLtJs0j5ah9de3ivmlfh3t5mVtFTNAOR2998D8Zqb5xR6/Is/PojyZtiO2ZbarbEW2gTaXrautiy3FFmuPtjvtEfYwu8Nut1vtqp3byR7rN9s9bvEXVLFW+afJVlX+KbFMO7lA8ee/DK8qZud0GRkxSjkvryxi5UbbeCofpxvHKl1+5hgxxrC4ipgRXU7lo4qMfu5yv80caeS5yw1bxVXeZsburUKpwRf5GY3y+pkpihYmi7/1aGa08J7kVmIsceE9VVWUED+7IKEgelBU/7KSc0B1EN0/XwmnJ1OMh8srvcZzKVVGjkiYKVXlxm3iL0FaeSQPLy1p5RFCVXlb1ToeWTpSlKt1JVUw2y/NsJsjYEYZQsHMXkS6MEM8KRJmWKMOu3TQYZcqFOwc4ZQu7dId4dJOZcKueY9eWtKs69ImjWiPtNmTRqfZYMeAW9Kcni6tXDrzCivmdenSsYtkQ5oGkyxNmjB818mGNCY7M3r+bJIWNOlzyqSP7EthP9toHTax3U/axHaHjfs/vGqL3KylV8O8HeKPa6pdpbWQamPJ7EkJxoJxut48ryH4Vzfp1ePGTxK6ptZocNWWGPNcJXpzrx3nqN4hqnu5SpppR+kob/MOT22Jr5enV6mrpqSqpSDfW3hGX4tP9eXNP0dj+aIxr+iroPAc1YWiukD0VSj6KhR9FXgKZF+lk8W+r/A226moqvjqDt3CQx3Yw9XJqVVF8c66QWJDtw5MTZiXvFkltpZC3VVGmKvICIeIqqzCrEJRhedMVEWIv6AKViXMG5iavJmtDVY5URzlKqKTU0vCqNzoM6LcSK0c4xVbxfDUnHvNZolLVidQ6eQS/EO+Xgru0y1p1jmv+nNdDQ0NswQ0uGcRlRuZleVG3xHwxGZDV9UlVSi7+GSZosiy5pCQUr/Zhko3nGD1ojuRcjM3ZtDjwKnLxpusTTYujgr1LUkpOdO34Q0+H4JzHJ/j6ymPz3xOS9c0cX6pb+nZp0PjuCq0Lyk1Bz205IEqdFqH9kRlIbEsbVnWsrymtKaspjwrSjetQaG2RrxKfT3XKFTvnnVyIpCsr8Jkwy3R39O+zimy4yaRcLur3LOYnK9fTjY7OemnJnZWsNVZsvn6kwvSUT4r2AhWoqP3hpO0hiBJVjZIUkcjHblT8POFHNH/A8tSsbIKZW5kc3RyZWFtCmVuZG9iago5IDAgb2JqCjw8L1R5cGUgL0ZvbnREZXNjcmlwdG9yCi9Gb250TmFtZSAvQUFBQUFBK0FyaWFsLUJvbGRNVAovRmxhZ3MgNAovQXNjZW50IDkwNS4yNzM0NAovRGVzY2VudCAtMjExLjkxNDA2Ci9TdGVtViA3Ni4xNzE4NzUKL0NhcEhlaWdodCA3MTUuODIwMzEKL0l0YWxpY0FuZ2xlIDAKL0ZvbnRCQm94IFstNjI3LjkyOTY5IC0zNzYuNDY0ODQgMjAwMCAxMDE3LjU3ODEzXQovRm9udEZpbGUyIDggMCBSPj4KZW5kb2JqCjEwIDAgb2JqCjw8L1R5cGUgL0ZvbnQKL0ZvbnREZXNjcmlwdG9yIDkgMCBSCi9CYXNlRm9udCAvQUFBQUFBK0FyaWFsLUJvbGRNVAovU3VidHlwZSAvQ0lERm9udFR5cGUyCi9DSURUb0dJRE1hcCAvSWRlbnRpdHkKL0NJRFN5c3RlbUluZm8gPDwvUmVnaXN0cnkgKEFkb2JlKQovT3JkZXJpbmcgKElkZW50aXR5KQovU3VwcGxlbWVudCAwPj4KL1cgWzAgWzc1MCAwIDAgMCAzMzMuMDA3ODFdIDM0IFs2MTAuODM5ODRdIDUwIFs3NzcuODMyMDNdIDc4IFs1NTYuMTUyMzQgMCAwIDAgNjEwLjgzOTg0XV0KL0RXIDA+PgplbmRvYmoKMTEgMCBvYmoKPDwvRmlsdGVyIC9GbGF0ZURlY29kZQovTGVuZ3RoIDI0OD4+IHN0cmVhbQp4nF2Qy2rDMBBF9/qKWaaLIFtxsjKCNG3Aiz6o2w+QpbErqCUhywv/ffUIKXRAgsOdOy966Z46owPQd29ljwFGbZTHxa5eIgw4aUNqBkrLcKP8y1k4QqO535aAc2dGS9oWgH5EdQl+g91Z2QEfCH3zCr02E+y+Ln3kfnXuB2c0ASrCOSgcY6UX4V7FjECzbd+pqOuw7aPnL+Nzcwgsc12mkVbh4oREL8yEpK1icGivMThBo/7px+IaRvktfM5uYnZVsZonYizT4ZrpUKgp1DxnOj1mOhbtVLrc6qV+6S73ZeTqfdwjHy8vkEbXBu/3ddYlV3q/Qa96RwplbmRzdHJlYW0KZW5kb2JqCjQgMCBvYmoKPDwvVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTAKL0Jhc2VGb250IC9BQUFBQUErQXJpYWwtQm9sZE1UCi9FbmNvZGluZyAvSWRlbnRpdHktSAovRGVzY2VuZGFudEZvbnRzIFsxMCAwIFJdCi9Ub1VuaWNvZGUgMTEgMCBSPj4KZW5kb2JqCnhyZWYKMCAxMgowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwMDAwMTUgMDAwMDAgbiAKMDAwMDAwMDQ1NSAwMDAwMCBuIAowMDAwMDAwMTc1IDAwMDAwIG4gCjAwMDAwMTAxNzcgMDAwMDAgbiAKMDAwMDAwMDIxMiAwMDAwMCBuIAowMDAwMDAwNjYzIDAwMDAwIG4gCjAwMDAwMDA3MTggMDAwMDAgbiAKMDAwMDAwMDc2NSAwMDAwMCBuIAowMDAwMDA5MzIwIDAwMDAwIG4gCjAwMDAwMDk1NTkgMDAwMDAgbiAKMDAwMDAwOTg1OCAwMDAwMCBuIAp0cmFpbGVyCjw8L1NpemUgMTIKL1Jvb3QgNyAwIFIKL0luZm8gMSAwIFI+PgpzdGFydHhyZWYKMTAzMjEKJSVFT0Y='
            ]
        ]);
    $orderId = $request->makeRequest($eushipments);

    // The ID of the order in the euShipments platform
    var_dump($orderId);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 15. Receiving the orders information

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\OrdersHistoryRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new OrdersHistoryRequest();
    $request->setTestMode(true)
        ->setOrders([
            'REF-1',
            'NOT-FOUND'
        ]);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 16. Calculate shipping price

To office:

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\ShipmentPriceRequest;
use BogdanKovachev\Eushipments\ReturnDocs;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new ShipmentPriceRequest();
    $request->setCourierId(328)
        ->setCompanyId(COMPANY_ID)
        ->setWeight(0.75)
        ->setCodAmount(100.0)
        ->setOpenPackage(true)
        ->setInsuranceAmount(100.0)
        ->setReturnDocs(ReturnDocs::NOTHING)
        ->setSaturdayDelivery(false)
        // ->setCity('Brasov')
        // ->setCounty('Brasov')
        ->setToOffice(true)
        ->setCurrency('RON');
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

To address:

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\ShipmentPriceRequest;
use BogdanKovachev\Eushipments\ReturnDocs;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new ShipmentPriceRequest();
    $request->setCourierId(328)
        ->setCompanyId(COMPANY_ID)
        ->setWeight(0.75)
        ->setCodAmount(100.0)
        ->setOpenPackage(true)
        ->setInsuranceAmount(100.0)
        ->setReturnDocs(ReturnDocs::NOTHING)
        ->setSaturdayDelivery(false)
        // ->setCity('Brno')
        // ->setCounty('Brno-Country District')
        ->setToOffice(false)
        ->setCurrency('CZK');
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 17. Create shipment

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Datastructure\Airwaybill;
use BogdanKovachev\Eushipments\Datastructure\Recipient;
use BogdanKovachev\Eushipments\Datastructure\Sender;
use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CreateAwbRequst;
use BogdanKovachev\Eushipments\Service;
use BogdanKovachev\Eushipments\ShipmentType;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $recipient = new Recipient();
    $recipient->setName('Test API')
        ->setCountryIsoCode('RO')
        // ->setCityId(20003314)
        ->setCityName('Brasov')
        ->setZipCode('500007')
        ->setRegion('Brasov')
        ->setStreetName('Bulevardul Eroilor 8')
        ->setBuildingNumber(8)
        ->setAddressText('fl 1')
        ->setContactPerson('Test API')
        ->setPhoneNumber('0888888888')
        ->setEmail('email@example.com');

    $awb = new Airwaybill();
    $awb->setShipmentType(ShipmentType::PACK)
        ->setParcels(1)
        ->setEnvelopes(0)
        ->setPallets(0)
        ->setTotalWeight(2.5)
        ->setDeclaredValue(100.00)
        ->setBankRepayment(100.00)
        ->setOtherRepayment('COD')
        ->setObservations('Additional info')
        ->setOpenPackage(false)
        ->setSaturdayDelivery(true)
        ->setReferenceNumber('REF-1')
        ->setProducts('Test 1 Test 2')
        ->setFragile(false)
        ->setProductsInfo('Products info')
        ->setPiecesInPack(2);
        // ->setPackages([])
        // ->setShipmentPayer('sender')

    $sender = new Sender();
    $sender->setName('Test API')
        ->setPhoneNumber('+359888888888')
        ->setEmail('email@example.com');

    $customsData = [
        'dutyPaymentInfo' => 'DDP',
        'customsValue' => 100.0
    ];

    $document = [
        'content' => 'JVBERi0xLjQKJdPr6eEKMSAwIG9iago8PC9UaXRsZSA8RkVGRjA0MUQwNDM1MDQzRTA0MzcwNDMwMDQzMzA0M0IwNDMwMDQzMjA0MzUwNDNEMDAyMDA0MzQwNDNFMDQzQTA0NDMwNDNDMDQzNTA0M0QwNDQyPgovUHJvZHVjZXIgKFNraWEvUERGIG0xMDMgR29vZ2xlIERvY3MgUmVuZGVyZXIpPj4KZW5kb2JqCjMgMCBvYmoKPDwvY2EgMQovQk0gL05vcm1hbD4+CmVuZG9iago1IDAgb2JqCjw8L0ZpbHRlciAvRmxhdGVEZWNvZGUKL0xlbmd0aCAxNzM+PiBzdHJlYW0KeJx1j00KAjEMhfc5RS5gJ0nTSQviQhhnrfQG6giCC8f7g+38LQSbkIR8vEfKSCV2XEpUwesL3uAsTNu1lyVjjUuP8zA+oOk9Pj5QuSVFZvE43mGA84+DSc3isSiOGZpTEahr6zPMA/B6hVhybeQY0KmlyOYT5hdU5p2YefUR8w33RF4OmJ/A7JSIiaw4zCTMJLoUpBUOG9BuARyYyeIG5J+CdAJdLt/6AgaoPNcKZW5kc3RyZWFtCmVuZG9iagoyIDAgb2JqCjw8L1R5cGUgL1BhZ2UKL1Jlc291cmNlcyA8PC9Qcm9jU2V0IFsvUERGIC9UZXh0IC9JbWFnZUIgL0ltYWdlQyAvSW1hZ2VJXQovRXh0R1N0YXRlIDw8L0czIDMgMCBSPj4KL0ZvbnQgPDwvRjQgNCAwIFI+Pj4+Ci9NZWRpYUJveCBbMCAwIDU5NiA4NDJdCi9Db250ZW50cyA1IDAgUgovU3RydWN0UGFyZW50cyAwCi9QYXJlbnQgNiAwIFI+PgplbmRvYmoKNiAwIG9iago8PC9UeXBlIC9QYWdlcwovQ291bnQgMQovS2lkcyBbMiAwIFJdPj4KZW5kb2JqCjcgMCBvYmoKPDwvVHlwZSAvQ2F0YWxvZwovUGFnZXMgNiAwIFI+PgplbmRvYmoKOCAwIG9iago8PC9MZW5ndGgxIDE3NDA0Ci9GaWx0ZXIgL0ZsYXRlRGVjb2RlCi9MZW5ndGggODQ2OT4+IHN0cmVhbQp4nO17eXwUVbb/ubeql3S2zkoWQlVokmCaSAiBsETSWUEjEAhiB0UTIAjKEiABRQVcUAioqKiIW8QBGUGpdBjssAxRxhnXAVxxGzIDKjrwABd0ULp+33vTICAPZvm9P977WMX5nruc773nLnWqbkKIEVEMQKXswSWlZczBkGEHUZo7uGJ4JcWRA/l85MMGV15R5Pi9bSnyW5HPHl7ZM+cmjz+WiK9Hvnp0yVDvaEvdfCJ9OVHU8vFTa+rYYPYG6r2oHzV+dr3+46BnPiJyHiGyjZlYd91U+4HN3YlCE4gsH1xXM6uOoigE7Q+BvfO6KTdNnH1kN/q2gq+2TZow9cZFx0LqiBIuJ7IPmVRbM6H9mvenon0d9n0noSB6Xkhn8Fcg323S1PobUzoro4mUjSi7fMr08TWp73Xrg3wY6tdOrbmxzp6vzkad8E+fVjO1NmFJ7znwBVk2um76rHozkz5A+kZRXzeztu7oZZ/FE+XA56i3ScydQ97iUshOHGlmmkiLukr6mvLpPrKi3Ek96QqMOp+/jOY5LORlZoj2z3GBbxsUGEbFTjp+/Phcpyw546oM9n8xbkvNzJpxpI+/aeYU0q+bWXsD6ZNqx80kfUpN/TTSf26TwoJpDq86ylTcjLriZpSDm9Ew3IxG4maSY4m9uXtyif/ayPzv7Il2SVu1Lz9F6DcqBm46fvynE06yd4NtyGlecjknRLHB2YhFPx7pgZgnlVKpgIbTDTQd80Wn58x9J++zxqwo7/KtmD27ZaWlN1xP7tDKbprIo+0WHmpTubhUOmu2hg4fNhyzkErTLe8ERrDetkHM5xELhZ7VdMtmsbqknka48j+4/3z+m438N+8DHTevtp41Lfw58p9zD/0PX+o+uv4/4Vv7U9X/L1/+mUudRYvPyO/DDsdjRmPEDlURdzCeB4JphidrUTDNsWMXBNMKDaCyYFrFns4Jpi2URAnBtBUpokKaSZOphqZQFhXRdOgJNJRG0WiqRc0s1E0n8Wz2wfObTf1g+TNDP40hrKZTPd1EdWDqdClNhc11sJwG1NG6fk7mSf1z3W9Rm4O+euHW4ckk2d4veyhGbibSAmtQ3uHpxbKfKbKPkSi7Dvx6jEPkaqHFqGYDJ8BSPP3Yk+wAni0LosLNm3lvCuU5HrfD4knUciMtmoVbxtj7WRVOIVbHfaEsNLFTkhKSbrWn29R0pqRz62a+nGx8uSeM6yyb3ccUlugI9TN7S+rn6xLc7mHfjs0fun+/81DHPcxZWlvy+VgqyC/IH+o88flYd69sVlZSVsKUqNQoRQBjl7LsIZ+wRDaXH2DewNoTCYG7WGLgC3iLnazeZXmNkkmjOzyZVmdETK7F6YzOHZAwINFjuSJuYtI6mzUkPoak/7DiNKZLv86Rfv6gz7kyzM/rPV1jWGdHZ6YRS8frKyTVGa/H8/ik1MhUp5M5E/XVxQnuYc5vxw6Fs8eA0f17HnLmO/OpoODEt/ulz9H9+/fKprFuN3Ol98nt2zevb5/cdFdXqy2tb7feOWpcrNVmtYqx8MMmG3ksIe2O6TcsSw4EQlny51+zLpPXV7lPiPHlhd3S9KrWa+DIhrm36i3HT6wdu+a+y8YEooOjrTK/sHxheYciMd5VnisesTxiXxG2IkK1M1uEPdKWkJFwY8icaNucqBvj7lIX2xeH3RWxMHpx7KK4RZ0WJdyVFGaLtsfakuKik2KTEuKSbDFZ4SGJWTYlPmODAw+O06E7FIefL/Xo2SmelOqUupQFKU0pVj3lSApPcWY0ERMTmI3HBUYtneft6JiXGUMPDXWOnXFMJKjgUMEhrODYsTNobExunpiK3jpFOSlVJxYb3TsnODNVxTnPX7e4hZWwhYF5gW2B1sA81uvz5uZ9n774Yjt/t31Fnc89IDAtsDLwRGA6dtGkfwQQ93/64UfxjkA8UK/FHg2lb1pJMT9tCY8qUPxmm+fWxKxcm+JUYqwZIROtGxzbHa+GvOH4yOGoVKoVHm5LCCmzXmmfbbVsCtmrHlJ/Ur+zWobZhtknWm9Vl6qPqY9bVlpX2lbaHZoabXWrbkumNdOWae8ZXq6WWxxWqy3EEWJ3WBwhilUNtahWjpAeGmq3Yd4coaqfT/UkWXra+2s2ZqsN56HpbAGJfUWJYQU3Bycrf+iJ/ETnsRkJh74N7qJ8oNj+0Z36322/1fkHe36zlReP8oqhveoLSc0ld5WcUszpTOyx3uKZSGX4Z4tajF1zKRsTeAjTuDvw3R2WzT8dY7MDt5y4hn26OLAee2akeUBdqQ6icEqkRzxDDrAv7N/HfB+n/okfsPDoREtiCK9yjo4ZHV+V8AhfYV1hfyTMH/Ie/9jySch7Ydhu1gPhzmftb/A3rTvsfwyzNNgXW++0K1F4cHyO0E7i+YlVbbH9bUnVyXXJPDkilRKTvIVysOKBmXHs1KagGWPZ2BnFXk/IZOfE6InxkxNUNraKUBiTG923dw7FxZKra7f0tNj4U9tkZOOJx4+y3MBrBx8IfN/I9IenTXvooWnTHuZdlzJrY+BPh48Gdtxprn1y7dqmx9euJU/hKE/BoEvyBw7o3y+vT27vnF7ZPS/O6uHOvKh7RnpaN1fXVF3rktI5OSkxoVN8XGxMdJQzMiI8LBSrarNaVAXr2aPUVVatG+nVhpruGjIkS+RdNSioOa2g2tBRVHamjaFXSzP9TEsPLCeeZenpsPScsmROPZ/ys3ropS7deKvEpfvZmBFepO8pcVXpxiGZHirTy2Q6HOnUVBD00oRJJbrBqvVSo2z2pMbS6hI01xzqKHYV1zqyelCzIxTJUKSMTq66ZtZpEJMJ3ql0QDPekeFwykhylZQaia4S4YGhpJXWTDAqRnhLS5JTU6uyehiseLxrnEGuIiPSLU2oWHZjWIsNm+xGnyxGQ0v05h5tjUv9ThpX7Q6b4JpQc7XXUGqqRB9RbvRbYnSauz/h5ywajy723n16bbLSWJowWRfZxsa7daNthPf02lSBVVVoA1yeVlbdWIaul2ISyyt19MYXVnkNthBd6mIkYlQd46t1lYqS6ut1I8RV5JrUeH01liap0aCRN6X6kpI8rWY7JZXqjaO8rlSjINlVVVPSuTmWGkfe1JLo0RPPrMnq0eyM6pjY5ojIYCIs/PRE7ak6mZLmIlU+8tTMMuGR61JsCEMfr8MTrwtj6iegth81ju8HM1xVDCxjAlZkshFSXN3oHCDKBd+wpDldeuN3hB3gOnTwzJKaYIk1zfkdiaTYJ6e2GupPpg2328jMFFvEVow1hY+DZL5PVo/Zft7XVefUoTB9VIG5raka0BPTn5oqFniJ30PjkDEWjPB25HUal+wjT093lcGrRU3byZq4K0TNgpM1p+jVLuzkjfL7OM6wp5/6F+mMjymdNMBg8eepru2oL690lY8Y49VLG6uDc1s+6oxcR32/U3XBlBFT7FWSeTDFkxVZi0159SljkfGGGWoa/lnlpp5gKNiUsoDpZYazekgHVjlSU/9bjt9mP43kN48IllQ/04JeGgPcZ+YHnpE/w7uwRgX+qum8fNSYxkbHGXVlCECNjWUuvayxurHGby4Y59KdrsZW/ix/trGutPrkgvrNzUuSjbKlVRjEJDYAm5VTUbOLLRrR7GGLKsd4W3GS1ReN8vo448XVRVXN3VDnbdWJPLKUnyoVOV3kqJxho/u4XVYlt3qIFshaVRbI/Hg/I1lmP1nGaLyfd5Q5ZRmuLMK7vrOUZ6mzmk6dcQLff1ICk839ok5o/hU+D1I6JHj5aD19wLoznVrYcepEP+CN2Qtf4yp9jw+ZDXSCHsJ5YBQ9zKKpG8XjtH8pU2HjpqXsMXO2+SVdgtPFKvNFdrv5HOrvoz/SD/DgLyqjPJy4r8BdS18qn+HrbCVOHHfjq2QgjWTx+AZ/H/d38OFBWk6/Z7eYP8izx+1oLx/f5IXmS+ZPlElL1WWWPSG/o/tpC7Oa483J1AWn+kbuNt8391I6TlvP0Hr45GZt6hCchm+ghbSCJSp/ROoh+g0FWBgfqxRbtqOnS3FOmUZzqJGeo9dZNKuw7LEcMW82v8DpJoa6w6fJ9CXrw4by1WqYOcj8iK6iVnoV4xV3m3qV+qzlqkCB+YT5MsXRi8zBtrKXLDmWe0/cZj5tvkBh8KcXZmQY+hlHd9BL9Bodpa/5fHM+DaFK9PwKS2E6S8eMv88T+Tw+T3kHJ4tCGgtvG+gpMrAim2kLbcPcfEzt9BmLZcnsMjaO3c++5mF8At+pPKZsVN5VmfpbzLeL0jBH9bSaNtGb9BbtZBYmDhYV7Ho2nT3CnmDt3OAH+feqXb1D/VE9YUkPtAd+NIeZ3+E8l0SX01yaj7l9hlpoI07y79HX9A0dY07Wj01iTzODtbODPIR35cN5HX+Yr+bPK8OU+5WX1D5qkXqD+pb6keUuyxJbjS3w05rAg4HnA7vNF83d2DsRaD8dp8rJdBt2xWraTu+g9Q/pU/qb2D9ofyAbw65BL7PYIracPc9eYbvZVxglybsrH8hL0Ot0PhPzdDt/kC9H7ztx7+If8U/53/l3ikXpqvRVZihPK4biV3Ypn6tONV29WO2lDlfHqCZWJscy2FJpWWtZZ3nZcsSab51grbMesN1uu9P+5onME38JUGBSwAi0YO/asZPmYiaepFXY9xuxBq9jRv8Mj9vpW6xCEr4oM+B3f1bGytlQdiW7mtWy29nd7AG2gj3GVrEXMAKMgdvgu5sX8kpew2v5nfxufg/fiHszf42/z/fwQ/C8k+JS3Eov5VJljHKVMg1jqFfmKXdiZu9XnlN2Ku8oXygHlENYtU5qF7VBnas+qj6rblR3Wy63TMW9yrLd0mbZbfnJ8pOVW5Osna09rddb11r/ZrPa+toqbItt79q+sdexziwTnuun/4SAJ+IZ7MKf47HqfHYIBSlMxZnpfnJjHSrxVHxDBUoA6xIh6uFbHE9UYwTT6lEN8OvZFurDXqH5Vq6IH7K2k499wtvVHfwSeg9v0ET1WWWa5XWeSusQjZbxrXwLK6KNPJ+P5o8rxD5ja+kz7PcbaTm7gc2idewQG8BuZXlsPr3L45VKdiflm6u4ykLw9X6E4AHdpk6ga87/kxDWnz6hLwNPquHqLYhPfnoYK7qe9rLf0nFmMQ8iuimIRjWIMkux3xeSiHpj8ZzNx/OYiAgyxbqTNjIrkS3POkidS0foH/SlZTN2VBEi6ReByeqT6j4zz8zCE4anjNbiuZtEg/HEfIZdsg15kbsaT7oDsSQHT3UFjaEJdCui3v2mYT5u3mHeZE6nN8A9znqw46wJT4QfjHx6Ffd99CFbgudw8L/2E6CTV2ACtdFXLIGlsRw8D4cssy3LLM9ZNlp+b3nL2guzfSc9hh39N+xmB0YwnnbTV/Q9s2NtEqkH5cLffvDdS1N4lbKNilkS1eGZ7Y44XhQcySy0cjtm73E8z9vwbBxBnLiafk97GGedMKLx6N+Odsoxz9fCeg1W8A7WgpIJiNqZ9HeMO4L14/Xoz4OWHkbUaoNPn9DnmG1T+tUDcaGEjUZb39OVNAE99KUK1owV2ET9EVlLlDcx392Yk4pYV/Yb8KrxhEZQCvW37GOcegSGmf34ZGUb3jEmypvw9kqmS9gMeBGJcZygODac+gRGwod3mKIa7G3pxaO81rxbmROYQm/Qb7EmHnW2rUROrPi5lzfULn5GrJx3BWxn5e0XsBUSFhLyL7cb8k+0G+4QP8BXz2P5S/8cF2hX2EeGhV6w3bP9Czun1c8+CHtnWNgF2z3bv/O1GxK0jwoPB1r/pXbD/4l2YyIjqON3Kv/9dbZ/keexDQ3axznxLfmLFT+/f84L+CDsE6KjL9ju2f5FX8AHYZ8cF0vn3+lYg7PycRfwQdinxMdfsN2z/Ys/j60zaK8niMf5fDtd/FLnzCvhAj6Il2NqYiJ1rOI/327iBdoV9mkpnS/Ybqez8inntOq44oL2mak6MOK87SadlU+9gA/C/uJurgu22/msfLcL+CDsczLS6fxPEJF2Vj7jAj4I+76ZF9Evd+iZ19njzjyPbZegPU6JdP4niMh1Vj7rPLZ60L44N4fO/wQRXXRWPvc8tmlB+/IB/ej8O53kr0BOvwacxzYzaF9ZOIh+uZPOvHqflS+8gA/C/uohpXT+nU7U76z8kAv4IOwnDCsnOvXL5XNfl5yVH3YBH4L2CilMXBZFwfcSowTLwdA2+sFuIszZzQBeLSFABzmAoRRqnkDwDgOGUzgwQmIkReBU7KRIYJTEaIoCxlA0MJZizB+xOQTGUxywE8UDE6iTeRzhJgGYJDGZEs1/4CFIAqZQMrALdQZqlALUqQswlTRgV9JxMncBv8eD2hWYRi5gOnUDZkjsTmnAiygdmEkZ5jFyU3ecMHtIzKJM4MXkBvakHsBsygL2kphDPc1vMf3ZwFzqBewD/Ib6Ug4wj3oD+1EusD/1AQ4Afk0DKc8U/w+hH/AS6g8cBDxKBTQA6MH54Si20SXmEXy7DgIWSyyhAmApeYBlVAgcLHEIFZuH6VIqMf+LLqNSYDmVAS+XOJQGA4fRpcDhdBmwgsqBI4AHaSRdbh7C2X4ocBQNA14hcTRVAK+kEUAvjYRlFVUCx0i8ikYBr6bR5t9xBrkSeI3Ea8kLrKYq8yucVMYAx9FVwPESJ9BYYC1dA5xI15pf0nUSJ1G1eUD8PhR4PY0H3kATgFMkTqVa4DSaCJxO15lf4Lt+EnAGTQbOpOvNz/GtfgOwnqYAGyTOpqnAOTTN/Axf/XXAm2gGcK7Em2km8BaaZe7H+aYeOE/ifJpt7qMFNAd4G90IvJ1uAt4h8U6aC1xIN5t/o7voVuDdwL/SIpoHXEzzgY20ALiEbgMulXgP3QG8l+4023FGWghcRncB75f4AN1t7qUHaRFwOTUCHwL+BWeLJcBHaClKVtA9wEfpXuBKiY/RMuDjOJftpSfoAfNTnPsFPkUPAptoOfBpehi4ih5BO89I/A2tQMlqehS4hlYCnwV+gpPf4+bHODU8gfRz9CRwHT0FXA/8mJ6nJuAL9DRwAz0DNOg3wGaJPlptfoRT3xrgRnrW/JB+J3ET/Rb4Ij0H9NM6YCutB24G7qEt9DxwK70A3EaG+QFOYQK3UzOwjXzAl6gF+DJtBO4Avk9/oE3AV+hF4B/JD/yTxFep1XyPXqPNwNdpC/AN2ma+i5OZwLfo98A/03bgTmoD7qKXgLvpZfMdept2AN+hP5hv07v0CvA9iegB+AH9CbiHXgN+SK8DPwLupo/pDeAn9CbwU3rL3EV/kbiXdgLbaRfwr7Qb+Dd629xJ+yTup3eAn9G7wM/pfeAXEg/QB+af6UvaA/yKPjTfwknzI+BB+hh4iD4B/hd9CjxMfwEeob3Ao8A3cXpvB35DfzXfoG9pH/A7icdoP/B7+gz4A30O/Ad9Yb5Ox+kA8Ef6EvgTfQU8QX8HBoCv4cR5EPhrTD9XTP9WxvRvZUz/9hcx/RsZ07/5RUz/Wsb0r2VM/1rG9KMyph+VMf2ojOlHZUw/+ouYfkTG9MMyph+WMf2wjOmHZUw/LGP6YRnTD8uYfljG9EO/xvR/K6bv+49j+l9lTP+rjOntMqa3y5jeLmP6XhnT9/4a0/+NmL71f3FMf+vXmP4/GtOPyZh+TMb0YzKmH5Mx/ZiM6cd+jen/52L6vl9j+q8x/deY3jxqQWG4sp42KOK/AjqBOqQJopBHWd9iC8/x+KGjY6X2xbtzWs02JAb0luVZy3MWbFXWIRj0RvE63xWieF2LpyRH6t4DO3TPXlL77B3VttgcrTAJtJ4QTpHB1HDIfZCnINshVji0jvZCTIiirFVW+co0tLAaDUUWxiqricHL1bQTYkIUeL8aY1lNh4MlKrx6piUkTHT/jGQlK8+AFQl0QhZANkB2Qiw0HfgUxIQoSK1C3SriyirlaZ9TcxY6lCdpPoQrKymSMbx02pQVLU45N4+2RMbkeAqdykNUAeFkKEOpDcLR7P2g3U8c5uW+rF5yCstbHBE5TtgvgdNL4MgSdNkEZDLvgQj7JS0x8aL5O3yRUZJ3sy87tyPR4kzIqcAs3EhMqVWm4cWnKfOgu0CPh06BHqdMwEtZ+OlpiXTmLEB/BTAvUOLwDtSUQiUebzVNKVGSxP+QhlmDL6KjnwZf98wcjLhYSZAmkUo4XnqaYldsvhxN36J45OQvagkJFf4t8jnjcrYpCxUbXu2asgBWnbTIbYoDK+uQIxnVEhKes6wwTBmFYY7CtGjwkWGWp8mGpvnQUGGUUqp0xgeBptygpODzQFPKlC5SP6s8jRehpjzRkt5Za9uiPChZD4hG0f2gjq01qCU8IqetMEQZhFpDuRcLcK/sfFlLer8cKkxXulM2hGOO5yM1X276RqQasWqNWKlGrFQjnGoUvzBTFqNmMWx6KnOpTplDyyBPIS22VZwPE9oqE92657QqiUoCJsa5BVPJUJrUEhIhPEvwRcdIs4SWsIicgm3KLOzzWWjTo9S3dErImb5FyZRD6dGSkCwIdT5s121Kp46lATFeLMk2pTMmQkxMitLFF6cZhRryYiNrxPjrfJeYJP4Of08st/gfIlK/EdRvBfWfO7TZxnd1PBT8baHbCzvzz9DYtRyvFaQ438J34NNH4x9xv/CCf8hb8cGi8T3IT4Buhe4NvdmX+qrm5/4WKPj+mC88XgyW7/C5ewYTWlow0Sk5mIiOzylM4y/zl/BVp/EPoLtBv8Tb8AWn8e3QCdBtvB7RXuO/433wDaXxjUH9B75VbHH+It+Ery2Nt/gihAuGzybUBp9VqBd81JGr6Klt5S/wdfiY1PjzvvQklK5tSe+mRW5Be4yv5vW+FC260MGfZl72LYyaaI/QFM1X+fJEI8t8W3WtlS/jyzwJeZ40T5ZnjZKdlp2VvUbR0/QsPU9foxc6+b0IIE9xPL98CTCPdI7dA/FAlvHFPjXPKDyBMYlxcVoAbJKpamCdTBHQear2iEwV8IU0HMLRxjzIfMgCyG2kAudCbobcArlVltRDGiBzEE3qwKgDow6MOsmoA6MOjDow6iSjTvbeABGMajCqwagGo1oyqsGoBqMajGrJEP5Wg1EtGRVgVIBRAUaFZFSAUQFGBRgVklEBRgUYFZLhAcMDhgcMj2R4wPCA4QHDIxkeMDxgeCQjG4xsMLLByJaMbDCywcgGI1syssHIBiNbMnQwdDB0MHTJ0MHQwdDB0CVDB0MHQ5cMJxhOMJxgOCXDCYYTDCcYTslwyvVpgAhGOxjtYLSD0S4Z7WC0g9EORrtktIPRDkY7n9Os7Cp8BZRdoOwCZZek7AJlFyi7QNklKbtA2QXKruDQ6+VkcGybeZD5kAUQwW0Dtw3cNnDbJLdNbq8GiOAaYBhgGGAYkmGAYYBhgGFIhgGGAYYhGU1gNIHRBEaTZDSB0QRGExhNktEkN24DRDD+9U35Ly8Nv4157XjX8gXsIqnn00Gp59EeqW+lZqlvoTVS30y3Sz2X8qSeQ+lSoz2p60mzM5+WF1kYjxAwHHItZDrkKcgGyHaITaZ2QvZCTN7H01WNtA23PWXbYNtus2ywtdt4pHW49SnrBut2q2WDtd3K9cJkHi7jKEIL3SdxPvAwBC8RYIFMFfBc9JuLONsHdy7P9UQd0g9nsp2ZbHsm25DJ7stkhSF8MFNlpNMpj8Nx5vWEpQ/S9kDy0jMGITLdu+lgJ82X3lfzs60d6iKPG/ogpBmyBnI7JA+SA8mCpEE0WZYJe6+na7DJrZAMSCpEF12Q/O8H0VF2TysPZ2taXgmnENFPRnfwtvgysqH8vozhUC/6MsZphSFsE2WIryL2O6zcOugNPm0/qp/vUOt92haotT4tF2qsL+NiqKt8GW9pheHsCtJUQR0V1JUYt9AjfdpomI3waRdBuX0Z6cIaJ3eWhtqLmBdf2ppIS1a3jp5cPm0gVFef1l9Y2ylDLDyzUpZ0zwIRWmmBQ4dbmVdlnlDtkPagdhD0v2NisT0+1P0q1M40PxvtcWhbs56EcaHmK3QIe7wfmoPaEPp32pq0xdpjaIulbdIe1S7W7s3y21F8D/xeLLvwabfrfr7OE6Mt0LK1+qz92iztMq1GG6mNTUO5T7ta2yrcpCrm5es2aRVo8FKMIs2nDU7zSxfLtJs0j5ah9de3ivmlfh3t5mVtFTNAOR2998D8Zqb5xR6/Is/PojyZtiO2ZbarbEW2gTaXrautiy3FFmuPtjvtEfYwu8Nut1vtqp3byR7rN9s9bvEXVLFW+afJVlX+KbFMO7lA8ee/DK8qZud0GRkxSjkvryxi5UbbeCofpxvHKl1+5hgxxrC4ipgRXU7lo4qMfu5yv80caeS5yw1bxVXeZsburUKpwRf5GY3y+pkpihYmi7/1aGa08J7kVmIsceE9VVWUED+7IKEgelBU/7KSc0B1EN0/XwmnJ1OMh8srvcZzKVVGjkiYKVXlxm3iL0FaeSQPLy1p5RFCVXlb1ToeWTpSlKt1JVUw2y/NsJsjYEYZQsHMXkS6MEM8KRJmWKMOu3TQYZcqFOwc4ZQu7dId4dJOZcKueY9eWtKs69ImjWiPtNmTRqfZYMeAW9Kcni6tXDrzCivmdenSsYtkQ5oGkyxNmjB818mGNCY7M3r+bJIWNOlzyqSP7EthP9toHTax3U/axHaHjfs/vGqL3KylV8O8HeKPa6pdpbWQamPJ7EkJxoJxut48ryH4Vzfp1ePGTxK6ptZocNWWGPNcJXpzrx3nqN4hqnu5SpppR+kob/MOT22Jr5enV6mrpqSqpSDfW3hGX4tP9eXNP0dj+aIxr+iroPAc1YWiukD0VSj6KhR9FXgKZF+lk8W+r/A226moqvjqDt3CQx3Yw9XJqVVF8c66QWJDtw5MTZiXvFkltpZC3VVGmKvICIeIqqzCrEJRhedMVEWIv6AKViXMG5iavJmtDVY5URzlKqKTU0vCqNzoM6LcSK0c4xVbxfDUnHvNZolLVidQ6eQS/EO+Xgru0y1p1jmv+nNdDQ0NswQ0uGcRlRuZleVG3xHwxGZDV9UlVSi7+GSZosiy5pCQUr/Zhko3nGD1ojuRcjM3ZtDjwKnLxpusTTYujgr1LUkpOdO34Q0+H4JzHJ/j6ymPz3xOS9c0cX6pb+nZp0PjuCq0Lyk1Bz205IEqdFqH9kRlIbEsbVnWsrymtKaspjwrSjetQaG2RrxKfT3XKFTvnnVyIpCsr8Jkwy3R39O+zimy4yaRcLur3LOYnK9fTjY7OemnJnZWsNVZsvn6kwvSUT4r2AhWoqP3hpO0hiBJVjZIUkcjHblT8POFHNH/A8tSsbIKZW5kc3RyZWFtCmVuZG9iago5IDAgb2JqCjw8L1R5cGUgL0ZvbnREZXNjcmlwdG9yCi9Gb250TmFtZSAvQUFBQUFBK0FyaWFsLUJvbGRNVAovRmxhZ3MgNAovQXNjZW50IDkwNS4yNzM0NAovRGVzY2VudCAtMjExLjkxNDA2Ci9TdGVtViA3Ni4xNzE4NzUKL0NhcEhlaWdodCA3MTUuODIwMzEKL0l0YWxpY0FuZ2xlIDAKL0ZvbnRCQm94IFstNjI3LjkyOTY5IC0zNzYuNDY0ODQgMjAwMCAxMDE3LjU3ODEzXQovRm9udEZpbGUyIDggMCBSPj4KZW5kb2JqCjEwIDAgb2JqCjw8L1R5cGUgL0ZvbnQKL0ZvbnREZXNjcmlwdG9yIDkgMCBSCi9CYXNlRm9udCAvQUFBQUFBK0FyaWFsLUJvbGRNVAovU3VidHlwZSAvQ0lERm9udFR5cGUyCi9DSURUb0dJRE1hcCAvSWRlbnRpdHkKL0NJRFN5c3RlbUluZm8gPDwvUmVnaXN0cnkgKEFkb2JlKQovT3JkZXJpbmcgKElkZW50aXR5KQovU3VwcGxlbWVudCAwPj4KL1cgWzAgWzc1MCAwIDAgMCAzMzMuMDA3ODFdIDM0IFs2MTAuODM5ODRdIDUwIFs3NzcuODMyMDNdIDc4IFs1NTYuMTUyMzQgMCAwIDAgNjEwLjgzOTg0XV0KL0RXIDA+PgplbmRvYmoKMTEgMCBvYmoKPDwvRmlsdGVyIC9GbGF0ZURlY29kZQovTGVuZ3RoIDI0OD4+IHN0cmVhbQp4nF2Qy2rDMBBF9/qKWaaLIFtxsjKCNG3Aiz6o2w+QpbErqCUhywv/ffUIKXRAgsOdOy966Z46owPQd29ljwFGbZTHxa5eIgw4aUNqBkrLcKP8y1k4QqO535aAc2dGS9oWgH5EdQl+g91Z2QEfCH3zCr02E+y+Ln3kfnXuB2c0ASrCOSgcY6UX4V7FjECzbd+pqOuw7aPnL+Nzcwgsc12mkVbh4oREL8yEpK1icGivMThBo/7px+IaRvktfM5uYnZVsZonYizT4ZrpUKgp1DxnOj1mOhbtVLrc6qV+6S73ZeTqfdwjHy8vkEbXBu/3ddYlV3q/Qa96RwplbmRzdHJlYW0KZW5kb2JqCjQgMCBvYmoKPDwvVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTAKL0Jhc2VGb250IC9BQUFBQUErQXJpYWwtQm9sZE1UCi9FbmNvZGluZyAvSWRlbnRpdHktSAovRGVzY2VuZGFudEZvbnRzIFsxMCAwIFJdCi9Ub1VuaWNvZGUgMTEgMCBSPj4KZW5kb2JqCnhyZWYKMCAxMgowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwMDAwMTUgMDAwMDAgbiAKMDAwMDAwMDQ1NSAwMDAwMCBuIAowMDAwMDAwMTc1IDAwMDAwIG4gCjAwMDAwMTAxNzcgMDAwMDAgbiAKMDAwMDAwMDIxMiAwMDAwMCBuIAowMDAwMDAwNjYzIDAwMDAwIG4gCjAwMDAwMDA3MTggMDAwMDAgbiAKMDAwMDAwMDc2NSAwMDAwMCBuIAowMDAwMDA5MzIwIDAwMDAwIG4gCjAwMDAwMDk1NTkgMDAwMDAgbiAKMDAwMDAwOTg1OCAwMDAwMCBuIAp0cmFpbGVyCjw8L1NpemUgMTIKL1Jvb3QgNyAwIFIKL0luZm8gMSAwIFI+PgpzdGFydHhyZWYKMTAzMjEKJSVFT0Y=',
        'format' => 'pdf'
    ];

    $request = new CreateAwbRequst();
    $request->setTestMode(true)
        ->setSenderId(COMPANY_ID)
        ->setCourierId(328)
        ->setWaybillAvailableDate('2022-10-15')
        ->setServiceName(Service::CROSSBORDER)
        ->setRecipient($recipient)
        ->setAwb($awb)
        ->setSender($sender)
        ->setDocument($document)
        ->setCustomsData($customsData)
        ->setCourierRequest([
            'date' => '2022-10-15',
            'timeFrom' => '09:00',
            'timeTo' => '15:00'
        ])
        ->setReturnLabel([
            'ndaysValid' => 0
        ]);
    $awbNumber = $request->makeRequest($eushipments);

    // The ID of the newly created AWB number in the euShipments platform
    var_dump($awbNumber);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 18. Get COD details by reference number

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\CheckCodByOrderRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new CheckCodByOrderRequest();
    $request->setReferenceNumber('REF-1')->setTestMode(true);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 19. Get PDF file as A4 or label

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\PrintRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new PrintRequest();
    $request->setAwbNumber('0000000000000')
        ->setTestMode(true)
        ->setPrintFileType(1);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 20. Get waybill (AWB) details

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\AwbDetailsRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new AwbDetailsRequest();
    $request->setTestMode(true)->setAwbNumber('0000000000000');
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 21. Get latest shipment status

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\ShipmentStatusRequest;
use BogdanKovachev\Eushipments\ShipmentStatus;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new ShipmentStatusRequest();
    $request->setTestMode(true)->setAwbNumber('0000000000000');
    $status = $request->makeRequest($eushipments);

    if ($status == ShipmentStatus::DELIVERED) {
        // Perform something
    }

    // The last status from the shipment tracking
    var_dump($status);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 22. Get complete shipment history

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\ShipmentHistoryRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new ShipmentHistoryRequest();
    $request->setTestMode(true)->setLanguage('BG')->setAwbNumber('0000000000000');
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 23. Get complete history of phone calls with client

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\PhoneCallHistoryRequest;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new PhoneCallHistoryRequest();
    $request->setTestMode(true)->setAwbNumber('0000000000000');
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 24. Get products availability

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Request\GetProductsAvailability;

$eushipments = new Eushipments();
$eushipments->setAuthToken(AUTH_TOKEN)->setSandboxMode(true);

try {
    $request = new GetProductsAvailability();
    $request->setTestMode(true)->setCompanyId(COMPANY_ID);
    $response = $request->makeRequest($eushipments);

    var_dump($response);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

### 26. Encoding and decoding files using the FileEncoder helper

Encode file (local or remote) to ready base64 encoded string for sending to euShipments API.

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\FileEncoder;

try {
    // Encoding remote file
    $base64encodedString = FileEncoder::encode('https://eushipments.com/test.pdf');

    // Encoding local file
    $base64encodedString = FileEncoder::encode('/Users/euShipments/test.pdf');
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```

Decoding base64 encoded string returned from euShipments API.

```php
require_once __DIR__ . '/vendor/autoload.php';

use BogdanKovachev\Eushipments\FileEncoder;

try {
    // Decoding local stored data
    $base64encodedString = @file_get_contents('/Users/euShipments/data.txt');
    $decoded = FileEncoder::decode($base64encodedString);

    // Decoding inline data
    $base64encodedString = 'JVBERi0xLjQKJdPr6eEKMSAwIG9iago8PC9UaXRsZSA8RkVGRjA0MUQwNDM1MDQzRTA0MzcwNDMwMDQzMzA0M0IwNDMwMDQzMjA0MzUwNDNEMDAyMDA0MzQwNDNFMDQzQTA0NDMwNDNDMDQzNTA0M0QwNDQyPgovUHJvZHVjZXIgKFNraWEvUERGIG0xMDMgR29vZ2xlIERvY3MgUmVuZGVyZXIpPj4KZW5kb2JqCjMgMCBvYmoKPDwvY2EgMQovQk0gL05vcm1hbD4+CmVuZG9iago1IDAgb2JqCjw8L0ZpbHRlciAvRmxhdGVEZWNvZGUKL0xlbmd0aCAxNzM+PiBzdHJlYW0KeJx1j00KAjEMhfc5RS5gJ0nTSQviQhhnrfQG6giCC8f7g+38LQSbkIR8vEfKSCV2XEpUwesL3uAsTNu1lyVjjUuP8zA+oOk9Pj5QuSVFZvE43mGA84+DSc3isSiOGZpTEahr6zPMA/B6hVhybeQY0KmlyOYT5hdU5p2YefUR8w33RF4OmJ/A7JSIiaw4zCTMJLoUpBUOG9BuARyYyeIG5J+CdAJdLt/6AgaoPNcKZW5kc3RyZWFtCmVuZG9iagoyIDAgb2JqCjw8L1R5cGUgL1BhZ2UKL1Jlc291cmNlcyA8PC9Qcm9jU2V0IFsvUERGIC9UZXh0IC9JbWFnZUIgL0ltYWdlQyAvSW1hZ2VJXQovRXh0R1N0YXRlIDw8L0czIDMgMCBSPj4KL0ZvbnQgPDwvRjQgNCAwIFI+Pj4+Ci9NZWRpYUJveCBbMCAwIDU5NiA4NDJdCi9Db250ZW50cyA1IDAgUgovU3RydWN0UGFyZW50cyAwCi9QYXJlbnQgNiAwIFI+PgplbmRvYmoKNiAwIG9iago8PC9UeXBlIC9QYWdlcwovQ291bnQgMQovS2lkcyBbMiAwIFJdPj4KZW5kb2JqCjcgMCBvYmoKPDwvVHlwZSAvQ2F0YWxvZwovUGFnZXMgNiAwIFI+PgplbmRvYmoKOCAwIG9iago8PC9MZW5ndGgxIDE3NDA0Ci9GaWx0ZXIgL0ZsYXRlRGVjb2RlCi9MZW5ndGggODQ2OT4+IHN0cmVhbQp4nO17eXwUVbb/ubeql3S2zkoWQlVokmCaSAiBsETSWUEjEAhiB0UTIAjKEiABRQVcUAioqKiIW8QBGUGpdBjssAxRxhnXAVxxGzIDKjrwABd0ULp+33vTICAPZvm9P977WMX5nruc773nLnWqbkKIEVEMQKXswSWlZczBkGEHUZo7uGJ4JcWRA/l85MMGV15R5Pi9bSnyW5HPHl7ZM+cmjz+WiK9Hvnp0yVDvaEvdfCJ9OVHU8vFTa+rYYPYG6r2oHzV+dr3+46BnPiJyHiGyjZlYd91U+4HN3YlCE4gsH1xXM6uOoigE7Q+BvfO6KTdNnH1kN/q2gq+2TZow9cZFx0LqiBIuJ7IPmVRbM6H9mvenon0d9n0noSB6Xkhn8Fcg323S1PobUzoro4mUjSi7fMr08TWp73Xrg3wY6tdOrbmxzp6vzkad8E+fVjO1NmFJ7znwBVk2um76rHozkz5A+kZRXzeztu7oZZ/FE+XA56i3ScydQ97iUshOHGlmmkiLukr6mvLpPrKi3Ek96QqMOp+/jOY5LORlZoj2z3GBbxsUGEbFTjp+/Phcpyw546oM9n8xbkvNzJpxpI+/aeYU0q+bWXsD6ZNqx80kfUpN/TTSf26TwoJpDq86ylTcjLriZpSDm9Ew3IxG4maSY4m9uXtyif/ayPzv7Il2SVu1Lz9F6DcqBm46fvynE06yd4NtyGlecjknRLHB2YhFPx7pgZgnlVKpgIbTDTQd80Wn58x9J++zxqwo7/KtmD27ZaWlN1xP7tDKbprIo+0WHmpTubhUOmu2hg4fNhyzkErTLe8ERrDetkHM5xELhZ7VdMtmsbqknka48j+4/3z+m438N+8DHTevtp41Lfw58p9zD/0PX+o+uv4/4Vv7U9X/L1/+mUudRYvPyO/DDsdjRmPEDlURdzCeB4JphidrUTDNsWMXBNMKDaCyYFrFns4Jpi2URAnBtBUpokKaSZOphqZQFhXRdOgJNJRG0WiqRc0s1E0n8Wz2wfObTf1g+TNDP40hrKZTPd1EdWDqdClNhc11sJwG1NG6fk7mSf1z3W9Rm4O+euHW4ckk2d4veyhGbibSAmtQ3uHpxbKfKbKPkSi7Dvx6jEPkaqHFqGYDJ8BSPP3Yk+wAni0LosLNm3lvCuU5HrfD4knUciMtmoVbxtj7WRVOIVbHfaEsNLFTkhKSbrWn29R0pqRz62a+nGx8uSeM6yyb3ccUlugI9TN7S+rn6xLc7mHfjs0fun+/81DHPcxZWlvy+VgqyC/IH+o88flYd69sVlZSVsKUqNQoRQBjl7LsIZ+wRDaXH2DewNoTCYG7WGLgC3iLnazeZXmNkkmjOzyZVmdETK7F6YzOHZAwINFjuSJuYtI6mzUkPoak/7DiNKZLv86Rfv6gz7kyzM/rPV1jWGdHZ6YRS8frKyTVGa/H8/ik1MhUp5M5E/XVxQnuYc5vxw6Fs8eA0f17HnLmO/OpoODEt/ulz9H9+/fKprFuN3Ol98nt2zevb5/cdFdXqy2tb7feOWpcrNVmtYqx8MMmG3ksIe2O6TcsSw4EQlny51+zLpPXV7lPiPHlhd3S9KrWa+DIhrm36i3HT6wdu+a+y8YEooOjrTK/sHxheYciMd5VnisesTxiXxG2IkK1M1uEPdKWkJFwY8icaNucqBvj7lIX2xeH3RWxMHpx7KK4RZ0WJdyVFGaLtsfakuKik2KTEuKSbDFZ4SGJWTYlPmODAw+O06E7FIefL/Xo2SmelOqUupQFKU0pVj3lSApPcWY0ERMTmI3HBUYtneft6JiXGUMPDXWOnXFMJKjgUMEhrODYsTNobExunpiK3jpFOSlVJxYb3TsnODNVxTnPX7e4hZWwhYF5gW2B1sA81uvz5uZ9n774Yjt/t31Fnc89IDAtsDLwRGA6dtGkfwQQ93/64UfxjkA8UK/FHg2lb1pJMT9tCY8qUPxmm+fWxKxcm+JUYqwZIROtGxzbHa+GvOH4yOGoVKoVHm5LCCmzXmmfbbVsCtmrHlJ/Ur+zWobZhtknWm9Vl6qPqY9bVlpX2lbaHZoabXWrbkumNdOWae8ZXq6WWxxWqy3EEWJ3WBwhilUNtahWjpAeGmq3Yd4coaqfT/UkWXra+2s2ZqsN56HpbAGJfUWJYQU3Bycrf+iJ/ETnsRkJh74N7qJ8oNj+0Z36322/1fkHe36zlReP8oqhveoLSc0ld5WcUszpTOyx3uKZSGX4Z4tajF1zKRsTeAjTuDvw3R2WzT8dY7MDt5y4hn26OLAee2akeUBdqQ6icEqkRzxDDrAv7N/HfB+n/okfsPDoREtiCK9yjo4ZHV+V8AhfYV1hfyTMH/Ie/9jySch7Ydhu1gPhzmftb/A3rTvsfwyzNNgXW++0K1F4cHyO0E7i+YlVbbH9bUnVyXXJPDkilRKTvIVysOKBmXHs1KagGWPZ2BnFXk/IZOfE6InxkxNUNraKUBiTG923dw7FxZKra7f0tNj4U9tkZOOJx4+y3MBrBx8IfN/I9IenTXvooWnTHuZdlzJrY+BPh48Gdtxprn1y7dqmx9euJU/hKE/BoEvyBw7o3y+vT27vnF7ZPS/O6uHOvKh7RnpaN1fXVF3rktI5OSkxoVN8XGxMdJQzMiI8LBSrarNaVAXr2aPUVVatG+nVhpruGjIkS+RdNSioOa2g2tBRVHamjaFXSzP9TEsPLCeeZenpsPScsmROPZ/ys3ropS7deKvEpfvZmBFepO8pcVXpxiGZHirTy2Q6HOnUVBD00oRJJbrBqvVSo2z2pMbS6hI01xzqKHYV1zqyelCzIxTJUKSMTq66ZtZpEJMJ3ql0QDPekeFwykhylZQaia4S4YGhpJXWTDAqRnhLS5JTU6uyehiseLxrnEGuIiPSLU2oWHZjWIsNm+xGnyxGQ0v05h5tjUv9ThpX7Q6b4JpQc7XXUGqqRB9RbvRbYnSauz/h5ywajy723n16bbLSWJowWRfZxsa7daNthPf02lSBVVVoA1yeVlbdWIaul2ISyyt19MYXVnkNthBd6mIkYlQd46t1lYqS6ut1I8RV5JrUeH01liap0aCRN6X6kpI8rWY7JZXqjaO8rlSjINlVVVPSuTmWGkfe1JLo0RPPrMnq0eyM6pjY5ojIYCIs/PRE7ak6mZLmIlU+8tTMMuGR61JsCEMfr8MTrwtj6iegth81ju8HM1xVDCxjAlZkshFSXN3oHCDKBd+wpDldeuN3hB3gOnTwzJKaYIk1zfkdiaTYJ6e2GupPpg2328jMFFvEVow1hY+DZL5PVo/Zft7XVefUoTB9VIG5raka0BPTn5oqFniJ30PjkDEWjPB25HUal+wjT093lcGrRU3byZq4K0TNgpM1p+jVLuzkjfL7OM6wp5/6F+mMjymdNMBg8eepru2oL690lY8Y49VLG6uDc1s+6oxcR32/U3XBlBFT7FWSeTDFkxVZi0159SljkfGGGWoa/lnlpp5gKNiUsoDpZYazekgHVjlSU/9bjt9mP43kN48IllQ/04JeGgPcZ+YHnpE/w7uwRgX+qum8fNSYxkbHGXVlCECNjWUuvayxurHGby4Y59KdrsZW/ix/trGutPrkgvrNzUuSjbKlVRjEJDYAm5VTUbOLLRrR7GGLKsd4W3GS1ReN8vo448XVRVXN3VDnbdWJPLKUnyoVOV3kqJxho/u4XVYlt3qIFshaVRbI/Hg/I1lmP1nGaLyfd5Q5ZRmuLMK7vrOUZ6mzmk6dcQLff1ICk839ok5o/hU+D1I6JHj5aD19wLoznVrYcepEP+CN2Qtf4yp9jw+ZDXSCHsJ5YBQ9zKKpG8XjtH8pU2HjpqXsMXO2+SVdgtPFKvNFdrv5HOrvoz/SD/DgLyqjPJy4r8BdS18qn+HrbCVOHHfjq2QgjWTx+AZ/H/d38OFBWk6/Z7eYP8izx+1oLx/f5IXmS+ZPlElL1WWWPSG/o/tpC7Oa483J1AWn+kbuNt8391I6TlvP0Hr45GZt6hCchm+ghbSCJSp/ROoh+g0FWBgfqxRbtqOnS3FOmUZzqJGeo9dZNKuw7LEcMW82v8DpJoa6w6fJ9CXrw4by1WqYOcj8iK6iVnoV4xV3m3qV+qzlqkCB+YT5MsXRi8zBtrKXLDmWe0/cZj5tvkBh8KcXZmQY+hlHd9BL9Bodpa/5fHM+DaFK9PwKS2E6S8eMv88T+Tw+T3kHJ4tCGgtvG+gpMrAim2kLbcPcfEzt9BmLZcnsMjaO3c++5mF8At+pPKZsVN5VmfpbzLeL0jBH9bSaNtGb9BbtZBYmDhYV7Ho2nT3CnmDt3OAH+feqXb1D/VE9YUkPtAd+NIeZ3+E8l0SX01yaj7l9hlpoI07y79HX9A0dY07Wj01iTzODtbODPIR35cN5HX+Yr+bPK8OU+5WX1D5qkXqD+pb6keUuyxJbjS3w05rAg4HnA7vNF83d2DsRaD8dp8rJdBt2xWraTu+g9Q/pU/qb2D9ofyAbw65BL7PYIracPc9eYbvZVxglybsrH8hL0Ot0PhPzdDt/kC9H7ztx7+If8U/53/l3ikXpqvRVZihPK4biV3Ypn6tONV29WO2lDlfHqCZWJscy2FJpWWtZZ3nZcsSab51grbMesN1uu9P+5onME38JUGBSwAi0YO/asZPmYiaepFXY9xuxBq9jRv8Mj9vpW6xCEr4oM+B3f1bGytlQdiW7mtWy29nd7AG2gj3GVrEXMAKMgdvgu5sX8kpew2v5nfxufg/fiHszf42/z/fwQ/C8k+JS3Eov5VJljHKVMg1jqFfmKXdiZu9XnlN2Ku8oXygHlENYtU5qF7VBnas+qj6rblR3Wy63TMW9yrLd0mbZbfnJ8pOVW5Osna09rddb11r/ZrPa+toqbItt79q+sdexziwTnuun/4SAJ+IZ7MKf47HqfHYIBSlMxZnpfnJjHSrxVHxDBUoA6xIh6uFbHE9UYwTT6lEN8OvZFurDXqH5Vq6IH7K2k499wtvVHfwSeg9v0ET1WWWa5XWeSusQjZbxrXwLK6KNPJ+P5o8rxD5ja+kz7PcbaTm7gc2idewQG8BuZXlsPr3L45VKdiflm6u4ykLw9X6E4AHdpk6ga87/kxDWnz6hLwNPquHqLYhPfnoYK7qe9rLf0nFmMQ8iuimIRjWIMkux3xeSiHpj8ZzNx/OYiAgyxbqTNjIrkS3POkidS0foH/SlZTN2VBEi6ReByeqT6j4zz8zCE4anjNbiuZtEg/HEfIZdsg15kbsaT7oDsSQHT3UFjaEJdCui3v2mYT5u3mHeZE6nN8A9znqw46wJT4QfjHx6Ffd99CFbgudw8L/2E6CTV2ACtdFXLIGlsRw8D4cssy3LLM9ZNlp+b3nL2guzfSc9hh39N+xmB0YwnnbTV/Q9s2NtEqkH5cLffvDdS1N4lbKNilkS1eGZ7Y44XhQcySy0cjtm73E8z9vwbBxBnLiafk97GGedMKLx6N+Odsoxz9fCeg1W8A7WgpIJiNqZ9HeMO4L14/Xoz4OWHkbUaoNPn9DnmG1T+tUDcaGEjUZb39OVNAE99KUK1owV2ET9EVlLlDcx392Yk4pYV/Yb8KrxhEZQCvW37GOcegSGmf34ZGUb3jEmypvw9kqmS9gMeBGJcZygODac+gRGwod3mKIa7G3pxaO81rxbmROYQm/Qb7EmHnW2rUROrPi5lzfULn5GrJx3BWxn5e0XsBUSFhLyL7cb8k+0G+4QP8BXz2P5S/8cF2hX2EeGhV6w3bP9Czun1c8+CHtnWNgF2z3bv/O1GxK0jwoPB1r/pXbD/4l2YyIjqON3Kv/9dbZ/keexDQ3axznxLfmLFT+/f84L+CDsE6KjL9ju2f5FX8AHYZ8cF0vn3+lYg7PycRfwQdinxMdfsN2z/Ys/j60zaK8niMf5fDtd/FLnzCvhAj6Il2NqYiJ1rOI/327iBdoV9mkpnS/Ybqez8inntOq44oL2mak6MOK87SadlU+9gA/C/uJurgu22/msfLcL+CDsczLS6fxPEJF2Vj7jAj4I+76ZF9Evd+iZ19njzjyPbZegPU6JdP4niMh1Vj7rPLZ60L44N4fO/wQRXXRWPvc8tmlB+/IB/ej8O53kr0BOvwacxzYzaF9ZOIh+uZPOvHqflS+8gA/C/uohpXT+nU7U76z8kAv4IOwnDCsnOvXL5XNfl5yVH3YBH4L2CilMXBZFwfcSowTLwdA2+sFuIszZzQBeLSFABzmAoRRqnkDwDgOGUzgwQmIkReBU7KRIYJTEaIoCxlA0MJZizB+xOQTGUxywE8UDE6iTeRzhJgGYJDGZEs1/4CFIAqZQMrALdQZqlALUqQswlTRgV9JxMncBv8eD2hWYRi5gOnUDZkjsTmnAiygdmEkZ5jFyU3ecMHtIzKJM4MXkBvakHsBsygL2kphDPc1vMf3ZwFzqBewD/Ib6Ug4wj3oD+1EusD/1AQ4Afk0DKc8U/w+hH/AS6g8cBDxKBTQA6MH54Si20SXmEXy7DgIWSyyhAmApeYBlVAgcLHEIFZuH6VIqMf+LLqNSYDmVAS+XOJQGA4fRpcDhdBmwgsqBI4AHaSRdbh7C2X4ocBQNA14hcTRVAK+kEUAvjYRlFVUCx0i8ikYBr6bR5t9xBrkSeI3Ea8kLrKYq8yucVMYAx9FVwPESJ9BYYC1dA5xI15pf0nUSJ1G1eUD8PhR4PY0H3kATgFMkTqVa4DSaCJxO15lf4Lt+EnAGTQbOpOvNz/GtfgOwnqYAGyTOpqnAOTTN/Axf/XXAm2gGcK7Em2km8BaaZe7H+aYeOE/ifJpt7qMFNAd4G90IvJ1uAt4h8U6aC1xIN5t/o7voVuDdwL/SIpoHXEzzgY20ALiEbgMulXgP3QG8l+4023FGWghcRncB75f4AN1t7qUHaRFwOTUCHwL+BWeLJcBHaClKVtA9wEfpXuBKiY/RMuDjOJftpSfoAfNTnPsFPkUPAptoOfBpehi4ih5BO89I/A2tQMlqehS4hlYCnwV+gpPf4+bHODU8gfRz9CRwHT0FXA/8mJ6nJuAL9DRwAz0DNOg3wGaJPlptfoRT3xrgRnrW/JB+J3ET/Rb4Ij0H9NM6YCutB24G7qEt9DxwK70A3EaG+QFOYQK3UzOwjXzAl6gF+DJtBO4Avk9/oE3AV+hF4B/JD/yTxFep1XyPXqPNwNdpC/AN2ma+i5OZwLfo98A/03bgTmoD7qKXgLvpZfMdept2AN+hP5hv07v0CvA9iegB+AH9CbiHXgN+SK8DPwLupo/pDeAn9CbwU3rL3EV/kbiXdgLbaRfwr7Qb+Dd629xJ+yTup3eAn9G7wM/pfeAXEg/QB+af6UvaA/yKPjTfwknzI+BB+hh4iD4B/hd9CjxMfwEeob3Ao8A3cXpvB35DfzXfoG9pH/A7icdoP/B7+gz4A30O/Ad9Yb5Ox+kA8Ef6EvgTfQU8QX8HBoCv4cR5EPhrTD9XTP9WxvRvZUz/9hcx/RsZ07/5RUz/Wsb0r2VM/1rG9KMyph+VMf2ojOlHZUw/+ouYfkTG9MMyph+WMf2wjOmHZUw/LGP6YRnTD8uYfljG9EO/xvR/K6bv+49j+l9lTP+rjOntMqa3y5jeLmP6XhnT9/4a0/+NmL71f3FMf+vXmP4/GtOPyZh+TMb0YzKmH5Mx/ZiM6cd+jen/52L6vl9j+q8x/deY3jxqQWG4sp42KOK/AjqBOqQJopBHWd9iC8/x+KGjY6X2xbtzWs02JAb0luVZy3MWbFXWIRj0RvE63xWieF2LpyRH6t4DO3TPXlL77B3VttgcrTAJtJ4QTpHB1HDIfZCnINshVji0jvZCTIiirFVW+co0tLAaDUUWxiqricHL1bQTYkIUeL8aY1lNh4MlKrx6piUkTHT/jGQlK8+AFQl0QhZANkB2Qiw0HfgUxIQoSK1C3SriyirlaZ9TcxY6lCdpPoQrKymSMbx02pQVLU45N4+2RMbkeAqdykNUAeFkKEOpDcLR7P2g3U8c5uW+rF5yCstbHBE5TtgvgdNL4MgSdNkEZDLvgQj7JS0x8aL5O3yRUZJ3sy87tyPR4kzIqcAs3EhMqVWm4cWnKfOgu0CPh06BHqdMwEtZ+OlpiXTmLEB/BTAvUOLwDtSUQiUebzVNKVGSxP+QhlmDL6KjnwZf98wcjLhYSZAmkUo4XnqaYldsvhxN36J45OQvagkJFf4t8jnjcrYpCxUbXu2asgBWnbTIbYoDK+uQIxnVEhKes6wwTBmFYY7CtGjwkWGWp8mGpvnQUGGUUqp0xgeBptygpODzQFPKlC5SP6s8jRehpjzRkt5Za9uiPChZD4hG0f2gjq01qCU8IqetMEQZhFpDuRcLcK/sfFlLer8cKkxXulM2hGOO5yM1X276RqQasWqNWKlGrFQjnGoUvzBTFqNmMWx6KnOpTplDyyBPIS22VZwPE9oqE92657QqiUoCJsa5BVPJUJrUEhIhPEvwRcdIs4SWsIicgm3KLOzzWWjTo9S3dErImb5FyZRD6dGSkCwIdT5s121Kp46lATFeLMk2pTMmQkxMitLFF6cZhRryYiNrxPjrfJeYJP4Of08st/gfIlK/EdRvBfWfO7TZxnd1PBT8baHbCzvzz9DYtRyvFaQ438J34NNH4x9xv/CCf8hb8cGi8T3IT4Buhe4NvdmX+qrm5/4WKPj+mC88XgyW7/C5ewYTWlow0Sk5mIiOzylM4y/zl/BVp/EPoLtBv8Tb8AWn8e3QCdBtvB7RXuO/433wDaXxjUH9B75VbHH+It+Ery2Nt/gihAuGzybUBp9VqBd81JGr6Klt5S/wdfiY1PjzvvQklK5tSe+mRW5Be4yv5vW+FC260MGfZl72LYyaaI/QFM1X+fJEI8t8W3WtlS/jyzwJeZ40T5ZnjZKdlp2VvUbR0/QsPU9foxc6+b0IIE9xPL98CTCPdI7dA/FAlvHFPjXPKDyBMYlxcVoAbJKpamCdTBHQear2iEwV8IU0HMLRxjzIfMgCyG2kAudCbobcArlVltRDGiBzEE3qwKgDow6MOsmoA6MOjDow6iSjTvbeABGMajCqwagGo1oyqsGoBqMajGrJEP5Wg1EtGRVgVIBRAUaFZFSAUQFGBRgVklEBRgUYFZLhAcMDhgcMj2R4wPCA4QHDIxkeMDxgeCQjG4xsMLLByJaMbDCywcgGI1syssHIBiNbMnQwdDB0MHTJ0MHQwdDB0CVDB0MHQ5cMJxhOMJxgOCXDCYYTDCcYTslwyvVpgAhGOxjtYLSD0S4Z7WC0g9EORrtktIPRDkY7n9Os7Cp8BZRdoOwCZZek7AJlFyi7QNklKbtA2QXKruDQ6+VkcGybeZD5kAUQwW0Dtw3cNnDbJLdNbq8GiOAaYBhgGGAYkmGAYYBhgGFIhgGGAYYhGU1gNIHRBEaTZDSB0QRGExhNktEkN24DRDD+9U35Ly8Nv4157XjX8gXsIqnn00Gp59EeqW+lZqlvoTVS30y3Sz2X8qSeQ+lSoz2p60mzM5+WF1kYjxAwHHItZDrkKcgGyHaITaZ2QvZCTN7H01WNtA23PWXbYNtus2ywtdt4pHW49SnrBut2q2WDtd3K9cJkHi7jKEIL3SdxPvAwBC8RYIFMFfBc9JuLONsHdy7P9UQd0g9nsp2ZbHsm25DJ7stkhSF8MFNlpNMpj8Nx5vWEpQ/S9kDy0jMGITLdu+lgJ82X3lfzs60d6iKPG/ogpBmyBnI7JA+SA8mCpEE0WZYJe6+na7DJrZAMSCpEF12Q/O8H0VF2TysPZ2taXgmnENFPRnfwtvgysqH8vozhUC/6MsZphSFsE2WIryL2O6zcOugNPm0/qp/vUOt92haotT4tF2qsL+NiqKt8GW9pheHsCtJUQR0V1JUYt9AjfdpomI3waRdBuX0Z6cIaJ3eWhtqLmBdf2ppIS1a3jp5cPm0gVFef1l9Y2ylDLDyzUpZ0zwIRWmmBQ4dbmVdlnlDtkPagdhD0v2NisT0+1P0q1M40PxvtcWhbs56EcaHmK3QIe7wfmoPaEPp32pq0xdpjaIulbdIe1S7W7s3y21F8D/xeLLvwabfrfr7OE6Mt0LK1+qz92iztMq1GG6mNTUO5T7ta2yrcpCrm5es2aRVo8FKMIs2nDU7zSxfLtJs0j5ah9de3ivmlfh3t5mVtFTNAOR2998D8Zqb5xR6/Is/PojyZtiO2ZbarbEW2gTaXrautiy3FFmuPtjvtEfYwu8Nut1vtqp3byR7rN9s9bvEXVLFW+afJVlX+KbFMO7lA8ee/DK8qZud0GRkxSjkvryxi5UbbeCofpxvHKl1+5hgxxrC4ipgRXU7lo4qMfu5yv80caeS5yw1bxVXeZsburUKpwRf5GY3y+pkpihYmi7/1aGa08J7kVmIsceE9VVWUED+7IKEgelBU/7KSc0B1EN0/XwmnJ1OMh8srvcZzKVVGjkiYKVXlxm3iL0FaeSQPLy1p5RFCVXlb1ToeWTpSlKt1JVUw2y/NsJsjYEYZQsHMXkS6MEM8KRJmWKMOu3TQYZcqFOwc4ZQu7dId4dJOZcKueY9eWtKs69ImjWiPtNmTRqfZYMeAW9Kcni6tXDrzCivmdenSsYtkQ5oGkyxNmjB818mGNCY7M3r+bJIWNOlzyqSP7EthP9toHTax3U/axHaHjfs/vGqL3KylV8O8HeKPa6pdpbWQamPJ7EkJxoJxut48ryH4Vzfp1ePGTxK6ptZocNWWGPNcJXpzrx3nqN4hqnu5SpppR+kob/MOT22Jr5enV6mrpqSqpSDfW3hGX4tP9eXNP0dj+aIxr+iroPAc1YWiukD0VSj6KhR9FXgKZF+lk8W+r/A226moqvjqDt3CQx3Yw9XJqVVF8c66QWJDtw5MTZiXvFkltpZC3VVGmKvICIeIqqzCrEJRhedMVEWIv6AKViXMG5iavJmtDVY5URzlKqKTU0vCqNzoM6LcSK0c4xVbxfDUnHvNZolLVidQ6eQS/EO+Xgru0y1p1jmv+nNdDQ0NswQ0uGcRlRuZleVG3xHwxGZDV9UlVSi7+GSZosiy5pCQUr/Zhko3nGD1ojuRcjM3ZtDjwKnLxpusTTYujgr1LUkpOdO34Q0+H4JzHJ/j6ymPz3xOS9c0cX6pb+nZp0PjuCq0Lyk1Bz205IEqdFqH9kRlIbEsbVnWsrymtKaspjwrSjetQaG2RrxKfT3XKFTvnnVyIpCsr8Jkwy3R39O+zimy4yaRcLur3LOYnK9fTjY7OemnJnZWsNVZsvn6kwvSUT4r2AhWoqP3hpO0hiBJVjZIUkcjHblT8POFHNH/A8tSsbIKZW5kc3RyZWFtCmVuZG9iago5IDAgb2JqCjw8L1R5cGUgL0ZvbnREZXNjcmlwdG9yCi9Gb250TmFtZSAvQUFBQUFBK0FyaWFsLUJvbGRNVAovRmxhZ3MgNAovQXNjZW50IDkwNS4yNzM0NAovRGVzY2VudCAtMjExLjkxNDA2Ci9TdGVtViA3Ni4xNzE4NzUKL0NhcEhlaWdodCA3MTUuODIwMzEKL0l0YWxpY0FuZ2xlIDAKL0ZvbnRCQm94IFstNjI3LjkyOTY5IC0zNzYuNDY0ODQgMjAwMCAxMDE3LjU3ODEzXQovRm9udEZpbGUyIDggMCBSPj4KZW5kb2JqCjEwIDAgb2JqCjw8L1R5cGUgL0ZvbnQKL0ZvbnREZXNjcmlwdG9yIDkgMCBSCi9CYXNlRm9udCAvQUFBQUFBK0FyaWFsLUJvbGRNVAovU3VidHlwZSAvQ0lERm9udFR5cGUyCi9DSURUb0dJRE1hcCAvSWRlbnRpdHkKL0NJRFN5c3RlbUluZm8gPDwvUmVnaXN0cnkgKEFkb2JlKQovT3JkZXJpbmcgKElkZW50aXR5KQovU3VwcGxlbWVudCAwPj4KL1cgWzAgWzc1MCAwIDAgMCAzMzMuMDA3ODFdIDM0IFs2MTAuODM5ODRdIDUwIFs3NzcuODMyMDNdIDc4IFs1NTYuMTUyMzQgMCAwIDAgNjEwLjgzOTg0XV0KL0RXIDA+PgplbmRvYmoKMTEgMCBvYmoKPDwvRmlsdGVyIC9GbGF0ZURlY29kZQovTGVuZ3RoIDI0OD4+IHN0cmVhbQp4nF2Qy2rDMBBF9/qKWaaLIFtxsjKCNG3Aiz6o2w+QpbErqCUhywv/ffUIKXRAgsOdOy966Z46owPQd29ljwFGbZTHxa5eIgw4aUNqBkrLcKP8y1k4QqO535aAc2dGS9oWgH5EdQl+g91Z2QEfCH3zCr02E+y+Ln3kfnXuB2c0ASrCOSgcY6UX4V7FjECzbd+pqOuw7aPnL+Nzcwgsc12mkVbh4oREL8yEpK1icGivMThBo/7px+IaRvktfM5uYnZVsZonYizT4ZrpUKgp1DxnOj1mOhbtVLrc6qV+6S73ZeTqfdwjHy8vkEbXBu/3ddYlV3q/Qa96RwplbmRzdHJlYW0KZW5kb2JqCjQgMCBvYmoKPDwvVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTAKL0Jhc2VGb250IC9BQUFBQUErQXJpYWwtQm9sZE1UCi9FbmNvZGluZyAvSWRlbnRpdHktSAovRGVzY2VuZGFudEZvbnRzIFsxMCAwIFJdCi9Ub1VuaWNvZGUgMTEgMCBSPj4KZW5kb2JqCnhyZWYKMCAxMgowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwMDAwMTUgMDAwMDAgbiAKMDAwMDAwMDQ1NSAwMDAwMCBuIAowMDAwMDAwMTc1IDAwMDAwIG4gCjAwMDAwMTAxNzcgMDAwMDAgbiAKMDAwMDAwMDIxMiAwMDAwMCBuIAowMDAwMDAwNjYzIDAwMDAwIG4gCjAwMDAwMDA3MTggMDAwMDAgbiAKMDAwMDAwMDc2NSAwMDAwMCBuIAowMDAwMDA5MzIwIDAwMDAwIG4gCjAwMDAwMDk1NTkgMDAwMDAgbiAKMDAwMDAwOTg1OCAwMDAwMCBuIAp0cmFpbGVyCjw8L1NpemUgMTIKL1Jvb3QgNyAwIFIKL0luZm8gMSAwIFI+PgpzdGFydHhyZWYKMTAzMjEKJSVFT0Y=';
    $decoded = FileEncoder::decode($base64encodedString);

    // Saving the decoded file locally
    file_put_contents('temp.pdf', $decoded);
} catch (TypeError $e) {
    echo 'Error: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
```
