<?php

namespace BogdanKovachev\Eushipments;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Eushipments {

    /**
     * API URL (Development)
     */
    const API_URL_DEVELOPMENT = 'https://test-api.inout.bg/api/v1';

    /**
     * API URL (Production)
     */
    const API_URL_PRODUCTION = 'https://api1.inout.bg/api/v1';

    /**
     * @var boolean
     */
    public $sandboxMode = false;

    /**
     * Authentication Token provided by euShipments. If you don't have one already, write to support@eushipments.com.
     *
     * @var string
     */
    public $authToken;

    /**
     * @param boolean $sandbox Sandbox mode
     * @return Eushipments
     */
    public function setSandboxMode(bool $sandbox): Eushipments {
        $this->sandboxMode = $sandbox;

        return $this;
    }

    /**
     * @param string $token Auth Token
     * @return Eushipments
     */
    public function setAuthToken(string $token): Eushipments {
        $this->authToken = $token;

        return $this;
    }

    /**
     * @return string
     */
    public function getApiUrl(): string {
        return $this->sandboxMode ? self::API_URL_DEVELOPMENT : self::API_URL_PRODUCTION;
    }
}
