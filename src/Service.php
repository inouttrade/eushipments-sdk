<?php

namespace BogdanKovachev\Eushipments;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Service {

    const CROSSBORDER = 'crossborder';
    const EUSHIPMENTS_AIR_EXPRESS = 'eushipmentsairexpress';
}
