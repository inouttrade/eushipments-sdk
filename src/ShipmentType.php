<?php

namespace BogdanKovachev\Eushipments;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class ShipmentType {

    const PACK = 'pack';
    const PALLET = 'pallet';
}
