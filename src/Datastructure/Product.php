<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Product {

    /**
     * Product ID
     *
     * @var integer
     */
    public $id;

    /**
     * Product name (up to 200 characters)
     *
     * @var string
     */
    public $name;

    /**
     * Product barcode (up to 20 characters)
     *
     * @var string
     */
    public $barcode;

    /**
     * (Optional) Product barcode type (up to 5 characters). By default it's EAN-13 (@see https://en.wikipedia.org/wiki/International_Article_Number).
     *
     * @var string|null
     */
    public $barcodeType;

    /**
     * (Optional) Product description (up to 500 characters)
     *
     * @var string|null
     */
    public $description;

    /**
     * Product length (in cm)
     *
     * @var float
     */
    public $length;

    /**
     * Product width (in cm)
     *
     * @var float
     */
    public $width;

    /**
     * Product height (in cm)
     *
     * @var float
     */
    public $height;

    /**
     * Product height (in kg)
     *
     * @var float
     */
    public $weight;

    /**
     * Reference number (up to 50 characters). The reference code must be unique for the client.
     *
     * @var string
     */
    public $referenceNumber;

    /**
     * Reference number (up to 50 characters). The reference code must be unique for the client.
     *
     * @var string
     */
    public $refNumber;

    /**
     * @param string $name
     * @return Product
     */
    function setName(string $name): Product {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $barcode
     * @return Product
     */
    function setBarcode(string $barcode): Product {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * @param string $barcodeType
     * @return Product
     */
    function setBarcodeType(string $barcodeType): Product {
        $this->barcodeType = $barcodeType;

        return $this;
    }

    /**
     * @param string $description
     * @return Product
     */
    function setDescription(string $description): Product {
        $this->description = $description;

        return $this;
    }

    /**
     * @param float $length
     * @return Product
     */
    function setLength(float $length): Product {
        $this->length = $length;

        return $this;
    }

    /**
     * @param float $width
     * @return Product
     */
    function setWidth(float $width): Product {
        $this->width = $width;

        return $this;
    }

    /**
     * @param float $height
     * @return Product
     */
    function setHeight(float $height): Product {
        $this->height = $height;

        return $this;
    }

    /**
     * @param float $weight
     * @return Product
     */
    function setWeight(float $weight): Product {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @param string $referenceNumber
     * @return Product
     */
    function setReferenceNumber(string $referenceNumber): Product {
        $this->referenceNumber = $referenceNumber;
        $this->refNumber = $referenceNumber;

        return $this;
    }

    /**
     * @param object $json
     * @return Product
     */
    public static function withJson(object $json): Product {
        $instance = new self();

        $instance->id = $json->ID;
        $instance->name = $json->NAME;
        $instance->description = $json->DESCRIPTION;
        $instance->length = $json->LENGTH_ITEM;
        $instance->width = $json->WIDTH;
        $instance->height = $json->HEIGHT;
        $instance->weight = $json->WEIGHT;
        $instance->referenceNumber = $json->REFERENCE_NUMBER;

        return $instance;
    }
}
