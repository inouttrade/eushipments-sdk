<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class RecipientFulfillment {

    /**
     * Name of recipient or company. Typically, both customer names (first name and last name) or company name are
     * filled in.
     *
     * @var string
     */
    public $name;

    /**
     * (Optional) The ID of a city where the shipment needs to delivery. Obtain an ID from Cities resource. Please for more
     * information, see Cities web service.
     *
     * The parameter is mandatory if both city and postcode are not provided.
     *
     * @var integer|null
     */
    public $cityId;

    /**
     * (Optional) Required field if city ID is not present. Recipient's city name e.g. София.
     *
     * @var string|null
     */
    public $cityName;

    /**
     * (Optional) Required field if a region is not present. Recipient's city zip code e.g. 1000.
     *
     * @var string|null
     */
    public $zipCode;

    /**
     * (Optional) Required field if a city zip code is not present. Recipient's region e.g. София.
     *
     * @var string|null
     */
    public $region;

    /**
     * (Optional) Obtain an ID from City offices or Courier offices resource. Please see one of the two web services for more
     * information.
     *
     * @var integer|null
     */
    public $officeId;

    /**
     * (Optional) Obtain a COURIER OFFICE CODE from City offices or Courier offices resource. Please see one of the two
     * web services for more information.
     *
     * @var string|null
     */
    public $officeCode;

    /**
     * Name of the street, office address or office name - e.g. If you create a shipment to address -  Main street or
     * Main street 10. If you want to create a shipment to an office and not use the "officeID" or "officeCode",
     * please add a keyword "to office: " to office name or office address (obtain information from "offices address"
     * web service.) e.g. " to office: Русе Николаевска" or " to office:  Русе ж.к. ЦЮР ул. Николаевска №109".
     *
     * @var string
     */
    public $streetName;

    /**
     * (Optional) Number of the street/building - e.g. 10
     *
     * @var integer|null
     */
    public $buildingNumber;

    /**
     * (Optional) Additional address information – e.g. бл.25 вх.1 ет.3 ап.4 or bl.25 vh.1 et.3 ap.4 or or another information for
     * the address.
     *
     * @var string|null
     */
    public $addressText;

    /**
     * (Optional) Contact person. Usually is the same as the name of recipient.
     *
     * @var string|null
     */
    public $contactPerson;

    /**
     * Recipient's phone number.
     *
     * @var string
     */
    public $phoneNumber;

    /**
     * (Optional) Country iso code. The parameter is mandatory, when the fields recipient.cityName and recipient.zipCode are filled
     * and validated.
     *
     * @var string|null
     */
    public $countryIsoCode;

    /**
     * (Optional) Recipient's email
     *
     * @var string|null
     */
    public $email;

    /**
     * @param string $name
     * @return RecipientFulfillment
     */
    function setName(string $name): RecipientFulfillment {
        $this->name = $name;

        return $this;
    }

    /**
     * @param integer $cityId
     * @return RecipientFulfillment
     */
    public function setCityId(int $cityId): RecipientFulfillment {
        $this->cityId = $cityId;

        return $this;
    }

    /**
     * @param string $cityName
     * @return RecipientFulfillment
     */
    function setCityName(string $cityName): RecipientFulfillment {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * @param string $zipCode
     * @return RecipientFulfillment
     */
    function setZipCode(string $zipCode): RecipientFulfillment {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @param string $region
     * @return RecipientFulfillment
     */
    public function setRegion(string $region): RecipientFulfillment {
        $this->region = $region;

        return $this;
    }

    /**
     * @param integer $officeId
     * @return RecipientFulfillment
     */
    public function setOfficeId(int $officeId): RecipientFulfillment {
        $this->officeId = $officeId;

        return $this;
    }

    /**
     * @param string $officeCode
     * @return RecipientFulfillment
     */
    public function setOfficeCode(string $officeCode): RecipientFulfillment {
        $this->officeCode = $officeCode;

        return $this;
    }

    /**
     * @param string $streetName
     * @return RecipientFulfillment
     */
    function setStreetName(string $streetName): RecipientFulfillment {
        $this->streetName = $streetName;

        return $this;
    }

    /**
     * @param integer $buildingNumber
     * @return RecipientFulfillment
     */
    function setBuildingNumber(int $buildingNumber): RecipientFulfillment {
        $this->buildingNumber = $buildingNumber;

        return $this;
    }

    /**
     * @param string $addressText
     * @return RecipientFulfillment
     */
    function setAddressText(string $addressText): RecipientFulfillment {
        $this->addressText = $addressText;

        return $this;
    }

    /**
     * @param string $contactPerson
     * @return RecipientFulfillment
     */
    function setContactPerson(string $contactPerson): RecipientFulfillment {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * @param string $phoneNumber
     * @return RecipientFulfillment
     */
    function setPhoneNumber(string $phoneNumber): RecipientFulfillment {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @param string $email
     * @return RecipientFulfillment
     */
    function setEmail(string $email): RecipientFulfillment {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $countryIsoCode
     * @return RecipientFulfillment
     */
    function setCountryIsoCode(string $countryIsoCode): RecipientFulfillment {
        $this->countryIsoCode = $countryIsoCode;

        return $this;
    }
}
