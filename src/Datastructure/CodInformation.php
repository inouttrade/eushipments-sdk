<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CodInformation {

    /**
     * AWB number
     *
     * @var string|null
     */
    public $awbNumber;

    /**
     * The value that you filled when creating the AWB
     *
     * @var string
     */
    public $referenceNumber;

    /**
     * COD amount in country currency
     *
     * @var float
     */
    public $cod;

    /**
     * If is set, then you will see the number of the protocol that InOut made the payment
     *
     * @var integer|null
     */
    public $payedNumber;

    /**
     * The date on which the payment was made
     *
     * @var string|null
     */
    public $payedDate;

    /**
     * @param object $json
     * @return CodInformation|null
     */
    public static function withJson(object $json): ?CodInformation {
        $instance = new self();

        // Workarround because of strange response
        if (!property_exists($json, 'awbNumber')) {
            return null;
        }

        $instance->awbNumber = $json->awbNumber;
        $instance->referenceNumber = $json->referenceNumber;
        $instance->cod = $json->COD;
        $instance->payedNumber = $json->payedNumber;
        $instance->payedDate = $json->payedDate;

        return $instance;
    }
}
