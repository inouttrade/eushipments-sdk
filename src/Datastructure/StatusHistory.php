<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class StatusHistory {

    /**
     * An ID of status
     *
     * @var integer
     */
    public $id;

    /**
     * The shipment status in our system
     *
     * @var string
     */
    public $status;

    /**
     * The date when status is created
     *
     * @var string
     */
    public $date;

    /**
     * Date when our partner has changed the status
     *
     * @var string
     */
    public $dateCreated;

    /**
     * The name of the status in the native language for every country
     *
     * @var string|null
     */
    public $comment;

    /**
     * Voucher number for the partner
     *
     * TODO: Validate the type
     *
     * @var string|null
     */
    public $partnerVoucher;

    /**
     * Note id is set if the partner has an additional note to shipment
     *
     * @var integer|null
     */
    public $noteId;

    /**
     * Note code is set if the partner has an additional note to shipment
     *
     * @var string|null
     */
    public $noteCode;

    /**
     * @param object $json
     * @return StatusHistory
     */
    public static function withJson(object $json): StatusHistory {
        $instance = new self();

        $instance->id = $json->ID;
        $instance->status = $json->STATUS;
        $instance->date = $json->DATE;
        $instance->dateCreated = $json->DATE_CREATED;
        $instance->comment = $json->COMMENT;
        $instance->partnerVoucher = $json->PARTNER_VOUCHER ?? null;
        $instance->noteId = $json->NOTE_ID ?? null;
        $instance->noteCode = $json->NOTE_CODE ?? null;

        return $instance;
    }
}
