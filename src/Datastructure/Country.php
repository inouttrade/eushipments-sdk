<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Country {

    /**
     * ID of country
     *
     * @var integer
     */
    public $id;

    /**
     * Country name
     *
     * @var string
     */
    public $name;

    /**
     * The name of the country in Cyrillic
     *
     * @var string
     */
    public $cyrilicName;

    /**
     * @param object $json
     * @return Country
     */
    public static function withJson(object $json): Country {
        $instance = new self();

        $instance->id = $json->ID;
        $instance->name = $json->NAME;
        $instance->cyrilicName = $json->CYRILLIC_NAME;

        return $instance;
    }
}
