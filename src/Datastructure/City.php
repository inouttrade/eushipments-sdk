<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class City {

    /**
     * The ID of country
     *
     * @var integer|null
     */
    public $countryId;

    /**
     * The ID of the county if exists
     *
     * @var integer|null
     */
    public $countyId;

    /**
     * An ID of the city
     *
     * @var integer
     */
    public $id;

    /**
     * Zip code of the city
     *
     * @var string
     */
    public $postalCode;

    /**
     * The city name on the local language
     *
     * @var string
     */
    public $cityNameLocal;

    /**
     * The name of the city with Latin symbols
     *
     * @var string|null
     */
    public $cityNameEn;

    /**
     * Region of location on local language
     *
     * @var string|null
     */
    public $county;

    /**
     * Region of location with Latin symbols
     *
     * @var string|null
     */
    public $countyEn;

    /**
     * This field used only for Bulgaria
     *
     * @var string|null
     */
    public $municipality;

    /**
     * If the country location has state this field is set it with local state name
     *
     * @var string|null
     */
    public $state;

    /**
     * If the country location has state this field is set it with latin characters state name
     *
     * @var string|null
     */
    public $stateEn;

    /**
     * If `true` the record was created through API
     *
     * @var string|null // TODO: Validate the type
     */
    public $creFromApi;

    /**
     * If this field is equal to `1`, the city is added through a cabinet in the nomenclature
     *
     * @var integer|null
     */
    public $manualEdit;

    /**
     * @param object $json
     * @param integer $type
     * @return City
     */
    public static function withJson(object $json, int $type = 1): City {
        $instance = new self();

        $instance->id = $json->ID;
        $instance->postalCode = $json->POSTAL_CODE;
        $instance->cityNameLocal = $json->CITY_NAME_LOCAL;
        $instance->county = $json->COUNTY;
        $instance->countyEn = $json->COUNTY_EN;
        $instance->state = $json->STATE;
        $instance->stateEn = $json->STATE_EN;

        if ($type === 1) { // Used in CitySuggestionsRequest.php
            $instance->countyId = $json->COUNTY_ID;
            $instance->countryId = $json->COUNTRY_ID;
            $instance->cityNameEn = $json->CITY_NAME_EN;
            $instance->creFromApi = $json->CRE_FROM_API;
        } else if ($type === 2) { // Used in CitiesRequest.php
            $instance->countyId = $json->COUNTY_ID;
            $instance->cityNameEn = $json->CITY_NAME_EN;
            $instance->manualEdit = $json->MANUAL_EDIT;
            $instance->municipality = $json->MUNICIPALITY;
        } else if ($type === 3) { // Used by CityRequest.php
            $instance->cityNameEn = $json->CITY_NAME;
            $instance->municipality = $json->MUNICIPALITY;
        }

        return $instance;
    }
}
