<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Office {

    /**
     * ID of office
     *
     * @var integer
     */
    public $id;

    /**
     * Office name
     *
     * @var string
     */
    public $office;

    /**
     * @param object $json
     * @return Office
     */
    public static function withJson(object $json): Office {
        $instance = new self();

        $instance->id = $json->ID;
        $instance->office = $json->OFFICE;

        return $instance;
    }
}
