<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Company {

    /**
     * An ID of the company
     *
     * @var integer
     */
    public $id;

    /**
     * Company name
     *
     * @var string
     */
    public $name;

    /**
     * Bulstat (VAT) number
     *
     * @var string|null
     */
    public $vat;

    /**
     * Company address
     *
     * @var string|null
     */
    public $address;

    /**
     * Owner of the company
     *
     * @var string|null
     */
    public $accountablePerson;

    /**
     * @param object $json
     * @return Company
     */
    public static function withJson(object $json): Company {
        $instance = new self();

        $instance->id = $json->ID;
        $instance->name = $json->NAME;
        $instance->vat = $json->BULSTAT;
        $instance->address = $json->ADDRESS;
        $instance->accountablePerson = $json->MOL;

        return $instance;
    }
}
