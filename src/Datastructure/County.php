<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class County {

    /**
     * ID of county
     *
     * @var integer|null
     */
    public $id;

    /**
     * County abbreviation
     *
     * @var string|null
     */
    public $abbreviation;

    /**
     * County name
     *
     * @var string
     */
    public $name;

    /**
     * @param object $json
     * @return County
     */
    public static function withJson(object $json): County {
        $instance = new self();

        $instance->id = $json->id;
        $instance->abbreviation = $json->abr;
        $instance->name = $json->name;

        return $instance;
    }
}
