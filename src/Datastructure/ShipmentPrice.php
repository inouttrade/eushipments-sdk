<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class ShipmentPrice {

    /**
     * Shipment price in Bulgarian lev (BGN)
     *
     * @var float
     */
    public $price;

    /**
     * Courier servic price - only for the weight of the shipment in Bulgarian lev (BGN)
     *
     * @var float
     */
    public $weight;

    /**
     * Cash on delivery price in Bulgarian lev (BGN)
     *
     * @var float
     */
    public $cod;

    /**
     * Insurance price in Bulgarian lev (BGN)
     *
     * @var float
     */
    public $insurance;

    /**
     * Check before pay price in Bulgarian lev (BGN)
     *
     * @var float
     */
    public $check;

    /**
     * Return documents price in Bulgarian lev (BGN)
     *
     * @var float
     */
    public $return;

    /**
     * Fulfill price in Bulgarian lev (BGN)
     *
     * @var float
     */
    public $fulfill;

    /**
     * Saturday delivery price in Bulgarian lev (BGN)
     *
     * @var float
     */
    public $saturdayDelivery;

    /**
     * SMS price in Bulgarian lev (BGN)
     *
     * @var float
     */
    public $sms;

    /**
     * @param object $json
     * @return ShipmentPrice
     */
    public static function withJson(object $json): ShipmentPrice {
        $instance = new self();

        $instance->price = floatval($json->price);
        $instance->weight = floatval($json->pr_weight);
        $instance->cod = floatval($json->pr_cod);
        $instance->insurance = floatval($json->pr_ins);
        $instance->check = floatval($json->pr_check);
        $instance->return = floatval($json->pr_ret);
        $instance->fulfill = floatval($json->pr_fulfill);
        $instance->saturdayDelivery = floatval($json->pr_sat_del);
        $instance->sms = floatval($json->pr_sms);

        return $instance;
    }
}
