<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Airwaybill {

    /**
     * Additional text to a recipient address.
     *
     * @var string
     */
    public $addressText;

    /**
     * The AWB status to this moment.
     *
     * @var string
     */
    public $awbStatus;

    /**
     * (Optional) COD amount of the shipment. You can set to `0` if the shipment doesn't have a COD amount.
     *
     * @var float|null
     */
    public $bankRepayment;

    /**
     * Building number or street number.
     *
     * @var string
     */
    public $buildingNumber;

    /**
     * An ID of the city from Cities Web Service. City, where the recipient should receive the shipment.
     *
     * @var integer
     */
    public $cityId;

    /**
     * COD amount
     *
     * @var float
     */
    public $cod;

    /**
     * (Optional) Contact person. Usually is the same as the name of recipient.
     *
     * @var string|null
     */
    public $contactPerson;

    /**
     * Country currency.
     *
     * @var string
     */
    public $currency;

    /**
     * (Optional) Insurance amount of the shipment. For some countries this field is boolean. The service returns an
     * error when filling more than of the permitted value for the country.
     *
     * @var float|boolean
     */
    public $declaredValue;

    /**
     * Number of envelopes in the shipment
     *
     * @var integer
     */
    public $envelopes;

    /**
     * (Optional) Marker whether the content is fragile. (fragile = 1 if the content is fragile). It's used only for
     * Bulgaria.
     *
     * @var boolean|null
     */
    public $fragile;

    /**
     * (Optional) Additional information about the shipment/products (notes)
     *
     * @var string|null
     */
    public $observations;

    /**
     * (Optional) Check up the shipment before pay
     *
     * @var boolean|null
     */
    public $openPackage;

    /**
     * (Optional) Additional information to COD
     *
     * @var string|null
     */
    public $otherRepayment;

    /**
     * (Optional) The number of pallets
     *
     * @var integer|null
     */
    public $pallets;

    /**
     * TODO: Ask euShipments because the property is used in examples but it's not documented
     *
     * @var array
     */
    public $packages = [];

    /**
     * Number of parcels in the shipment
     *
     * @var integer
     */
    public $parcels;

    /**
     * Phone number of the recipient
     *
     * @var string
     */
    public $phoneNumber;

    /**
     * (Optional) Products amount in one shipment. e.g If the shipment contain 5 products the field will be filled with 5.
     *
     * @var integer|null
     */
    public $piecesInPack;

    /**
     * (Optional) Аdditional product information
     *
     * @var string|null
     */
    public $productsInfo;

    /**
     * (Optional) The content of the shipment. e.g. product1 product2. It'e required only for Bulgaria.
     *
     * @var string|null
     */
    public $products;

    /**
     * Names of recipient / company.
     *
     * @var string
     */
    public $recipientName;

    /**
     * Client reference number. Usually, the client put here the unique number of order from the site or unique order
     * number of the content or invoice number. Helps for tracking and reporting.
     * The information in the field must be unique to avoid duplicate shipments!
     *
     * @var string
     */
    public $referenceNumber;

    /**
     * (Optional) Saturday delivery. Not available for some countries.
     *
     * @var boolean|null
     */
    public $saturdayDelivery;

    /**
     * The ID of company, obtained from `CompaniesRequest`
     *
     * @var integer
     */
    public $senderId;

    /**
     * TODO: Ask euShipments because the property is used in examples but it's not documented
     *
     * @var string
     */
    public $shipmentPayer;

    /**
     * (Optional) Shipment type - `pack` or `pallet`
     *
     * @var string|null
     */
    public $shipmentType;

    /**
     * Recipient street name.
     *
     * @var string
     */
    public $streetName;

    /**
     * Total weight of the shipment in kg
     *
     * @var float
     */
    public $totalWeight;

    /**
     * The date on which the shipment is to be dispatched.
     *
     * @var string
     */
    public $waybillAvailableDate;

    /**
     * @param float $bankRepayment
     * @return Airwaybill
     */
    function setBankRepayment(float $bankRepayment): Airwaybill {
        $this->bankRepayment = $bankRepayment;

        return $this;
    }

    /**
     * @param float|boolean $declaredValue
     * @return Airwaybill
     */
    function setDeclaredValue($declaredValue): Airwaybill {
        $this->declaredValue = $declaredValue;

        return $this;
    }

    /**
     * @param integer $envelopes
     * @return Airwaybill
     */
    function setEnvelopes(int $envelopes): Airwaybill {
        $this->envelopes = $envelopes;

        return $this;
    }

    /**
     * @param boolean $fragile
     * @return Airwaybill
     */
    function setFragile(bool $fragile): Airwaybill {
        $this->fragile = $fragile;

        return $this;
    }

    /**
     * @param string $observations
     * @return Airwaybill
     */
    function setObservations(string $observations): Airwaybill {
        $this->observations = $observations;

        return $this;
    }

    /**
     * @param boolean $openPackage
     * @return Airwaybill
     */
    function setOpenPackage(bool $openPackage): Airwaybill {
        $this->openPackage = $openPackage;

        return $this;
    }

    /**
     * @param string $otherRepayment
     * @return Airwaybill
     */
    function setOtherRepayment(string $otherRepayment): Airwaybill {
        $this->otherRepayment = $otherRepayment;

        return $this;
    }

    /**
     * @param integer $pallets
     * @return Airwaybill
     */
    public function setPallets(int $pallets): Airwaybill {
        $this->pallets = $pallets;

        return $this;
    }

    /**
     * @param integer $parcels
     * @return Airwaybill
     */
    function setParcels(int $parcels): Airwaybill {
        $this->parcels = $parcels;

        return $this;
    }

    /**
     * @param integer $piecesInPack
     * @return Airwaybill
     */
    function setPiecesInPack(int $piecesInPack): Airwaybill {
        $this->piecesInPack = $piecesInPack;

        return $this;
    }

    /**
     * @param string $products
     * @return Airwaybill
     */
    function setProducts(string $products): Airwaybill {
        $this->products = $products;

        return $this;
    }

    /**
     * @param string $productsInfo
     * @return Airwaybill
     */
    function setProductsInfo(string $productsInfo): Airwaybill {
        $this->productsInfo = $productsInfo;

        return $this;
    }

    /**
     * @param string $referenceNumber
     * @return Airwaybill
     */
    function setReferenceNumber(string $referenceNumber): Airwaybill {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * @param boolean $saturdayDelivery
     * @return Airwaybill
     */
    function setSaturdayDelivery(bool $saturdayDelivery): Airwaybill {
        $this->saturdayDelivery = $saturdayDelivery;

        return $this;
    }

    /**
     * @param string $shipmentType
     * @return Airwaybill
     */
    public function setShipmentType(string $shipmentType): Airwaybill {
        $this->shipmentType = $shipmentType;

        return $this;
    }

    /**
     * @param float $totalWeight
     * @return Airwaybill
     */
    function setTotalWeight(float $totalWeight): Airwaybill {
        $this->totalWeight = $totalWeight;

        return $this;
    }

    /**
     * @param object $json
     * @return Airwaybill
     */
    public static function withJson(object $json): Airwaybill {
        $instance = new self();

        $instance->senderId = $json->SENDER_ID;
        $instance->waybillAvailableDate = $json->WAYBILL_AVAILABLE_DATE;
        $instance->recipientName = $json->RECIPIENT_NAME;
        $instance->cityId = $json->CITY_ID;
        $instance->streetName = $json->STREET_NAME;
        $instance->buildingNumber = $json->BUILDING_NUMBER;
        $instance->addressText = $json->ADDRESS_TEXT;
        $instance->contactPerson = $json->CONTACT_PERSON;
        $instance->phoneNumber = $json->PHONE_NUMBER;
        $instance->parcels = $json->PARCELS;
        $instance->envelopes = $json->ENVELOPES;
        $instance->totalWeight = $json->TOTAL_WEIGHT;
        $instance->declaredValue = $json->DECLARED_VALUE;
        $instance->cod = $json->COD;
        $instance->otherRepayment = $json->OTHER_REPAYMENT;
        $instance->currency = $json->CURRENCY;
        $instance->openPackage = $json->OPEN_PACKAGE;
        $instance->saturdayDelivery = $json->SATURDAY_DELIVERY;
        $instance->observations = $json->OBSERVATIONS;
        $instance->awbStatus = $json->AWB_STATUS;

        return $instance;
    }
}
