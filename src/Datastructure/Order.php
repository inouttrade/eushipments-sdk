<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Order {

    /**
     * Reference number
     *
     * @var string
     */
    public $referenceNumber;

    /**
     * AWB number
     *
     * @var string|null
     */
    public $awb;

    /**
     * A field that contains the text from a possible error while executing the web service from the server-side.
     *
     * @var string|null
     */
    public $error;

    /**
     * Error code if any
     *
     * @var integer|null
     */
    public $errorCode;

    /**
     * Array with statuses history
     *
     * @var array
     */
    public $statusesHistory;

    /**
     * @param object $json
     * @return Order
     */
    public static function withJson(object $json): Order {
        $instance = new self();

        $instance->referenceNumber = $json->refNum;
        $instance->awb = $json->awb;
        $instance->error = $json->error ?? null;
        $instance->errorCode = $json->errorCode ?? null;

        foreach ($json->statusesHistory as $statusHistory) {
            $instance->statusesHistory[] = StatusHistory::withJson($statusHistory);
        }

        return $instance;
    }
}
