<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Courier {

    /**
     * ID of courier
     *
     * @var integer
     */
    public $id;

    /**
     * The courier name
     *
     * @var string
     */
    public $name;

    /**
     * If this field is `true`, you can create shipments to an office
     *
     * @var boolean
     */
    public $toOffice;

    /**
     * If this field is `true`, you can create shipments to an address
     *
     * @var boolean
     */
    public $toAddress;

    /**
     * @param object $json
     * @return Courier
     */
    public static function withJson(object $json): Courier {
        $instance = new self();

        $instance->id = $json->ID;
        $instance->name = $json->NAME;
        $instance->toOffice = $json->TO_OFFICE;
        $instance->toAddress = $json->TO_ADDRESS;

        return $instance;
    }
}
