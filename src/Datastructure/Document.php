<?php

namespace BogdanKovachev\Eushipments\Datastructure;

use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Document {

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $fileContains;

    /**
     * @param object $json
     * @return Document
     */
    public static function withJson(object $json): Document {
        $instance = new self();

        $instance->type = $json->type;

        $temp = base64_decode($json->awb_print, true);
        if ($temp === false) {
            throw new Exception('The returned file data is corrupted.');
        } else {
            $instance->fileContains = $temp;
        }

        return $instance;
    }
}
