<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Sender {

    /**
     * (Optional) Name of sender
     *
     * @var string|null
     */
    public $name;

    /**
     * (Optional) Phone number of sender
     *
     * @var string|null
     */
    public $phoneNumber;

    /**
     * (Optional) Email of sender
     *
     * @var string|null
     */
    public $email;

    /**
     * @param string $name
     * @return Sender
     */
    public function setName(string $name): Sender {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $phoneNumber
     * @return Sender
     */
    public function setPhoneNumber(string $phoneNumber): Sender {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @param string $email
     * @return Sender
     */
    public function setEmail(string $email): Sender {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string|null $name
     * @param string|null $phoneNumber
     * @param string|null $email
     */
    function __construct(string $name = null, string $phoneNumber = null, string $email = null) {
        if ($name !== null) {
            $this->name = $name;
        }

        if ($phoneNumber !== null) {
            $this->phoneNumber = $phoneNumber;
        }

        if ($email !== null) {
            $this->email = $email;
        }
    }
}
