<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class ProductAvailability {

    /**
     * Дата към която се показва наличността. Наличността се опреснява един път на ден в 03:00 към показаната дата.
     *
     * @var string
     */
    public $availabilityDate;

    /**
     * Име на склада, в която е наличността. Може да е на euShipments или партьорски фулфилмънт склад.
     *
     * @var string
     */
    public $storeName;

    /**
     * Име на артукил
     *
     * @var string
     */
    public $name;

    /**
     * Описание на артикула (дълго)
     *
     * @var string|null
     */
    public $description;

    /**
     * Описание на артикула (кратко)
     *
     * @var string|null
     */
    public $shortDescription;

    /**
     * Име на клиента, към който е закачен артикула. Ако към потребителя има повече от един клиент се виждат всички.
     *
     * @var string
     */
    public $customer;

    /**
     * Референтен номер на артикула
     *
     * @var string
     */
    public $referenceNumber;

    /**
     * Код на артикула в партьорския склад
     *
     * @var string|null
     */
    public $courierItemCode;

    /**
     * Налично количество
     *
     * @var integer|float
     */
    public $availableQuantity;

    /**
     * @param object $json
     * @return ProductAvailability
     */
    public static function withJson(object $json): ProductAvailability {
        $instance = new self();

        $instance->availabilityDate = $json->AVAIL_DATE;
        $instance->storeName = $json->STORE_NAME;
        $instance->name = $json->ART_NAME;
        $instance->description = $json->ART_LNG_DESC;
        $instance->shortDescription = $json->ART_SH_DESC;
        $instance->customer = $json->CUST_NAME;
        $instance->referenceNumber = $json->REFERENCE_NUMBER;
        $instance->courierItemCode = $json->COURIER_ITEM_CODE;
        $instance->availableQuantity = $json->AVAIL_QTY;

        return $instance;
    }
}
