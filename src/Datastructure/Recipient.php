<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Recipient {

    /**
     * Name of recipient or company. Typically, both customer names (first name and last name) or company name are
     * filled in.
     *
     * @var string
     */
    public $name;

    /**
     * (Optional) The ID of a city where the shipment needs to delivery. Obtain an ID from Cities resource. Please for more
     * information, see Cities web service.
     *
     * @var integer|null
     */
    public $cityId;

    /**
     * (Optional) Country 2 digits ISO code (HU, BG, RO, etc.). Needs to be sent with `cityName` and `zipCode`
     *
     * @var string|null
     */
    public $countryIsoCode;

    /**
     * (Optional) City name where the shipment needs to be delivered. If you have your own nomenclature with а cities,
     * you can fill in the fields cityName, zipCode and region.
     *
     * @var string|null
     */
    public $cityName;

    /**
     * (Optional) Postal code of the city.  Needs to be sent with `countryIsoCode` and `cityName`.
     *
     * @var string|null
     */
    public $zipCode;

    /**
     * (Optianl) Region
     *
     * @var string|null
     */
    public $region;

    /**
     * Name of the street, office address or office name - e.g. If you create shipment to address -  Main street or Main
     * street 10. If you want to create shipment to an office, please add a keyword "to office: "to office name or
     * office address (obtain information from "offices address" web service.) e.g. "to office: Русе Николаевска" or
     * "to office: Русе ж.к. ЦЮР ул. Николаевска №109".
     *
     * @var string
     */
    public $streetName;

    /**
     * (Optional) Number of the street/building - e.g. 10
     *
     * @var integer|null
     */
    public $buildingNumber;

    /**
     * (Optional) Additional address information – e.g. бл.25 ет 3. or bl.25 et. 3
     *
     * @var string|null
     */
    public $addressText;

    /**
     * (Optional) Contact person. Usually is the same as the name of recipient.
     *
     * @var string|null
     */
    public $contactPerson;

    /**
     * Phone number of the recipient
     *
     * @var string
     */
    public $phoneNumber;

    /**
     * (Optioanl) Email of the recipient. Some couriers may return error if not provided.
     *
     * @var string|null
     */
    public $email;

    /**
     * @param string $name
     * @return Recipient
     */
    function setName(string $name): Recipient {
        $this->name = $name;

        return $this;
    }

    /**
     * @param integer $cityId
     * @return Recipient
     */
    function setCityId(int $cityId): Recipient {
        $this->cityId = $cityId;

        return $this;
    }

    /**
     * @param string $countryIsoCode
     * @return Recipient
     */
    function setCountryIsoCode(string $countryIsoCode): Recipient {
        $this->countryIsoCode = $countryIsoCode;

        return $this;
    }

    /**
     * @param string $cityName
     * @return Recipient
     */
    function setCityName(string $cityName): Recipient {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * @param string $zipCode
     * @return Recipient
     */
    function setZipCode(string $zipCode): Recipient {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @param string $region
     * @return Recipient
     */
    function setRegion(string $region): Recipient {
        $this->region = $region;

        return $this;
    }

    /**
     * @param string $streetName
     * @return Recipient
     */
    function setStreetName(string $streetName): Recipient {
        $this->streetName = $streetName;

        return $this;
    }

    /**
     * @param integer $buildingNumber
     * @return Recipient
     */
    function setBuildingNumber(int $buildingNumber): Recipient {
        $this->buildingNumber = $buildingNumber;

        return $this;
    }

    /**
     * @param string $addressText
     * @return Recipient
     */
    function setAddressText(string $addressText): Recipient {
        $this->addressText = $addressText;

        return $this;
    }

    /**
     * @param string $contactPerson
     * @return Recipient
     */
    function setContactPerson(string $contactPerson): Recipient {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * @param string $phoneNumber
     * @return Recipient
     */
    function setPhoneNumber(string $phoneNumber): Recipient {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @param string $email
     * @return Recipient
     */
    function setEmail(string $email): Recipient {
        $this->email = $email;

        return $this;
    }
}
