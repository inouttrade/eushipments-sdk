<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Action {

    /**
     * The ID of the call
     *
     * @var integer
     */
    public $id;

    /**
     * Аction that was taken after the call
     *
     * @var string
     */
    public $action;

    /**
     * Comment of the person who contacted the recipient
     *
     * @var string
     */
    public $comment;

    /**
     * Date of action.
     *
     * @var string
     */
    public $date;

    /**
     * @param object $json
     * @return Action
     */
    public static function withJson(object $json): Action {
        $instance = new self();

        $instance->id = $json->ID;
        $instance->action = $json->ACTION;
        $instance->comment = $json->COMMENT;
        $instance->date = $json->Date;

        return $instance;
    }
}
