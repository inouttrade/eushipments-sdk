<?php

namespace BogdanKovachev\Eushipments\Datastructure;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CourierOffice {

    /**
     * ID of office
     *
     * @var integer
     */
    public $id;

    /**
     * The ID of the city where is the office
     *
     * @var integer
     */
    public $cityId;

    /**
     * Name of the office
     *
     * @var string
     */
    public $officeName;

    /**
     * Name of city where is the office
     *
     * @var string
     */
    public $cityName;

    /**
     * Office address
     *
     * @var string
     */
    public $address;

    /**
     * Office latitude
     *
     * @var string
     */
    public $latitude;

    /**
     * Office longitude
     *
     * @var string
     */
    public $longitude;

    /**
     * End work time for the office during the week
     *
     * @var string
     */
    public $workEnd;

    /**
     * Begin work time for the office during the week
     *
     * @var string
     */
    public $workBegin;

    /**
     * End work time for the office on Saturday
     *
     * @var string
     */
    public $workBeginSaturday;

    /**
     * Begin work time for the office on Saturday.
     *
     * @var string
     */
    public $workEndSaturday;

    /**
     * The ID of the courier.
     *
     * @var integer|null
     */
    public $courierId;

    /**
     * The office ID of the subcontractor.
     *
     * @var integer|null
     */
    public $courierOfficeId;

    /**
     * The office code of the subcontractor.
     *
     * @var string|null
     */
    public $courierOfficeCode;

    /**
     * City id by global Romania nomenclature number.
     *
     * @var integer|null
     */
    public $groupCityId;

    /**
     * Quarter ID
     *
     * @var integer|null
     */
    public $quarterId;

    /**
     * Steet ID.
     *
     * @var integer|null
     */
    public $streetId;

    /**
     * Street name.
     *
     * @var string|null
     */
    public $streetName;

    /**
     * Street number.
     *
     * @var string|null
     */
    public $streetNumber;

    /**
     * Date of creation.
     *
     * @var string|null
     */
    public $createdAt;

    /**
     * Date of update.
     *
     * @var string|null
     */
    public $updatedAt;

    /**
     * Name of neighborhood.
     *
     * @var string|null
     */
    public $quarterName;

    /**
     * Date and time of deletion.
     *
     * @var string|null
     */
    public $deletedAt;

    /**
     * Тrue if the office is a locker or station.
     *
     * @var boolean|null
     */
    public $isStation;

    /**
     * Zip code of the city. For Romania, missing postal codes for some large cities.
     *
     * @var string
     */
    public $postcode;

    /**
     * Region. For some countries it is null, the nomenclature does not support data for the region.
     *
     * @var string|null
     */
    public $region;

    /**
     * @param object $json
     * @param integer $type
     * @return ?CourierOffice
     */
    public static function withJson(object $json, int $type = 1): ?CourierOffice {
        $instance = new self();

        // Workarround because of strange response
        if (!property_exists($json, 'ID')) {
            return null;
        }

        $instance->id = $json->ID;
        $instance->cityId = $json->CITY_ID;
        $instance->officeName = $json->OFFICE_NAME;
        $instance->cityName = $json->CITY_NAME;
        $instance->address = $json->ADDRESS;
        $instance->latitude = $json->LATITUDE;
        $instance->longitude = $json->LONGITUDE;
        $instance->workEnd = $json->WORK_END;
        $instance->workBegin = $json->WORK_BEGIN;
        $instance->workBeginSaturday = $json->WORK_BEGIN_SATURDAY;
        $instance->workEndSaturday = $json->WORK_END_SATURDAY;
        $instance->courierId = $json->COURIER_ID;
        $instance->courierOfficeId = $json->COURIER_OFFICE_ID;
        $instance->courierOfficeCode = $json->COURIER_OFFICE_CODE;
        $instance->groupCityId = $json->GROUP_CITY_ID;

        if ($type === 1) { // Used by OfficeAddressesRequest.php
            $instance->quarterId = $json->ID_QUARTER;
            $instance->streetId = $json->ID_STREET;
            $instance->streetName = $json->STREET_NAME;
            $instance->streetNumber = $json->STREET_NUM;
            $instance->createdAt = $json->CREATED_AT;
            $instance->updatedAt = $json->UPDATED_AT;
            $instance->quarterName = $json->QUARTER_NAME;
            $instance->deletedAt = $json->DELETED_AT;
            $instance->isStation = $json->IS_STATION;
        } else if ($type === 2) { // Used by CourierOfficesRequest.php
            $instance->postcode = $json->POST_CODE;
            $instance->region = $json->REGION;
        }

        return $instance;
    }
}
