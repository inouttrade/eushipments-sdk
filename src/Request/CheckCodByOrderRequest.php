<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Datastructure\CodInformation;
use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CheckCodByOrderRequest extends Request {

    /**
     * The value you filled in the "referenceNumber" when creating the AWB. The value can be an order number, a product
     * number or an invoice number.
     *
     * @var string
     */
    public $referenceNumber;

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * @param string $referenceNumber
     * @return CheckCodByOrderRequest
     */
    public function setReferenceNumber(string $referenceNumber): CheckCodByOrderRequest {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * @param boolean $testMode
     * @return CheckCodByOrderRequest
     */
    public function setTestMode(bool $testMode): CheckCodByOrderRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return CodInformation
     */
    public function makeRequest(Eushipments $eushipments): CodInformation {
        $url = $eushipments->getApiUrl() . '/check-cod-by-order/' . $this->referenceNumber;

        $queryParams = [
            'testMode' => $this->testMode ? 1 : 0
        ];

        $rawResponse = $this->sendRequest($url, 'GET', $queryParams, $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $information = CodInformation::withJson($response);
        if ($information === null) {
            throw new Exception('AWB with the `referenceNumber` provided not found.');
        }

        return $information;
    }
}
