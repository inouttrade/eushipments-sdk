<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Datastructure\Airwaybill;
use BogdanKovachev\Eushipments\Datastructure\RecipientFulfillment;
use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CreateOrderRequest extends Request {

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * ID of company, you can receive it from the „Companies Web Service“.
     *
     * @var integer
     */
    public $senderId;

    /**
     * ID of courier, you can receive it from the „Couriers Web Service“.
     * Only required if you have more than one courier for the country you want to create an order.
     *
     * @var integer|null
     */
    public $courierId;

    /**
     * Date when the order should be processed.
     *
     * @var string
     */
    public $waybillAvailableDate;

    /**
     * (Optional) The service name must be one of the following:
     *  - crossborder
     *  - eushipmentsairexpress
     * @see Service.php
     *
     * The property is mandatory if the service name is airexpress
     *
     * @var string|null
     */
    public $serviceName;

    /**
     * @var RecipientFulfillment
     */
    public $recipient;

    /**
     * @var Airwaybill Airway bill
     */
    public $awb;

    /**
     * @var array
     */
    public $products;

    /**
     * @var array
     */
    public $customsData;

    /**
     * @param boolean $testMode
     * @return CreateOrderRequest
     */
    public function setTestMode(bool $testMode): CreateOrderRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param integer $senderId
     * @return CreateOrderRequest
     */
    public function setSenderId(int $senderId): CreateOrderRequest {
        $this->senderId = $senderId;

        return $this;
    }

    /**
     * @param integer $courierId
     * @return CreateOrderRequest
     */
    public function setCourierId(int $courierId): CreateOrderRequest {
        $this->courierId = $courierId;

        return $this;
    }

    /**
     * @param string $waybillAvailableDate
     * @return CreateOrderRequest
     */
    public function setWaybillAvailableDate(string $waybillAvailableDate): CreateOrderRequest {
        $this->waybillAvailableDate = $waybillAvailableDate;

        return $this;
    }

    /**
     * @param string $serviceName
     * @return CreateOrderRequest
     */
    public function setServiceName(string $serviceName): CreateOrderRequest {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * @param RecipientFulfillment $recipient
     * @return CreateOrderRequest
     */
    public function setRecipient(RecipientFulfillment $recipient): CreateOrderRequest {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * @param Airwaybill $awb
     * @return CreateOrderRequest
     */
    public function setAwb(Airwaybill $awb): CreateOrderRequest {
        $this->awb = $awb;

        return $this;
    }

    /**
     * @param array $products
     * @return CreateOrderRequest
     */
    public function setProducts(array $products): CreateOrderRequest {
        $this->products = $products;

        return $this;
    }

    /**
     * @param array $customsData
     * @return CreateOrderRequest
     */
    public function setCustomsData(array $customsData): CreateOrderRequest {
        $this->customsData = $customsData;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return string
     */
    public function makeRequest(Eushipments $eushipments): string {
        $url = $eushipments->getApiUrl() . '/fulfilment/create-order';

        $bodyParams = [
            'testMode' => $this->testMode ? 1 : 0,
            'senderId' => $this->senderId,
            'waybillAvailableDate' => $this->waybillAvailableDate,
            'recipient' => $this->recipient,
            'awb' => $this->awb,
            'products' => $this->products
        ];

        if ($this->courierId !== null) {
            $bodyParams['courierId'] = $this->courierId;
        }

        if ($this->serviceName !== null) {
            $bodyParams['serviceName'] = $this->serviceName;
        }

        if ($this->customsData !== null) {
            $bodyParams['customsData'] = $this->customsData;
        }

        $rawResponse = $this->sendRequest($url, 'POST', [], $eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error') && !empty($response->error)) {
            throw new Exception($response->error);
        }

        return $response->orderId;
    }
}
