<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\Country;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CountriesRequest extends Request {

    /**
     * @param Eushipments $eushipments
     * @return Country[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/get-countries';

        $rawResponse = $this->sendRequest($url, 'GET', [], $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $countries = [];
        foreach ($response as $value) {
            $countries[] = Country::withJson($value);
        }

        return $countries;
    }
}
