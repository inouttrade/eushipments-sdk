<?php

namespace BogdanKovachev\Eushipments\Request;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class Request {

    /**
     * TODO: Reorder the function parameters
     *
     * @param string $url
     * @param string $httpMethod
     * @param array $queryParams
     * @param string $authToken
     * @param array $bodyParams
     */
    function sendRequest(string $url, string $httpMethod, array $queryParams = [], string $authToken, array $bodyParams = []) {
        $curl = curl_init();

        if (!empty($queryParams)) {
            $url .= '?' . http_build_query($queryParams);
        }

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 5,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $httpMethod,
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer ' . $authToken,
                'Cache-Control: no-cache'
            ]
        ]);

        if (!empty($bodyParams)) {
            curl_setopt_array($curl, [
                CURLOPT_POSTFIELDS => json_encode($bodyParams),
                CURLOPT_HTTPHEADER => [
                    'Authorization: Bearer ' . $authToken,
                    'Cache-Control: no-cache',
                    'Content-Type: application/json'
                ]
            ]);
        }

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }
}
