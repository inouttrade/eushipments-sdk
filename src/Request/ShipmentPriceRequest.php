<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\ShipmentPrice;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class ShipmentPriceRequest extends Request {

    /**
     * Courier ID, obtained from `CouriersRequest` or euShipments website
     *
     * @var integer
     */
    public $courierId;

    /**
     * Company ID, obtained from `CompaniesRequest` or euShipments website
     *
     * @var integer
     */
    public $companyId;

    /**
     * Shipment weight
     *
     * @var float
     */
    public $weight;

    /**
     * Shipment COD amount in local currency. Set to `0` if the shipment doesn't have COD.
     *
     * @var float
     */
    public $codAmount = 0.0;

    /**
     * Check up the package before pay
     *
     * @var boolean
     */
    public $openPackage;

    /**
     * Shipment insurance amount in Bulgarian lev (BGN). Set to `0` if the shipment doesn't have insurance.
     *
     * @var float
     */
    public $insuranceAmount = 0.0;

    /**
     * What type of documents should be returned after successful delivery. All the available options are listed in
     * `ReturnDocs`.
     *
     * @var integer
     */
    public $returnDocs;

    /**
     * Indicates that the package should be delivered Saturday
     *
     * @var boolean
     */
    public $saturdayDelivery;

    /**
     * City where the shipment is to be delivered. Required only for urgent delivery in Romania.
     *
     * @var string
     */
    public $city;

    /**
     * County where the shipment is to be delivered. Required only for urgent delivery in Romania.
     *
     * @var string
     */
    public $county;

    /**
     * Indicates that the package should be delivered to office. Required only if the courier is different from Urgent.
     *
     * @var boolean
     */
    public $toOffice;

    /**
     * Currency in ISO 4217 format. Required only if the courier is different from Urgent.
     *
     * @var string
     */
    public $currency;

    /**
     * @param integer $courierId
     * @return ShipmentPriceRequest
     */
    public function setCourierId(int $courierId): ShipmentPriceRequest {
        $this->courierId = $courierId;

        return $this;
    }

    /**
     * @param integer $companyId
     * @return ShipmentPriceRequest
     */
    public function setCompanyId(int $companyId): ShipmentPriceRequest {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * @param float $weight
     * @return ShipmentPriceRequest
     */
    public function setWeight(float $weight): ShipmentPriceRequest {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @param float $codAmount
     * @return ShipmentPriceRequest
     */
    public function setCodAmount(float $codAmount): ShipmentPriceRequest {
        $this->codAmount = $codAmount;

        return $this;
    }

    /**
     * @param boolean $openPackage
     * @return ShipmentPriceRequest
     */
    public function setOpenPackage(bool $openPackage): ShipmentPriceRequest {
        $this->openPackage = $openPackage;

        return $this;
    }

    /**
     * @param float $insuranceAmount
     * @return ShipmentPriceRequest
     */
    public function setInsuranceAmount(float $insuranceAmount): ShipmentPriceRequest {
        $this->insuranceAmount = $insuranceAmount;

        return $this;
    }

    /**
     * @param integer $returnDocs
     * @return ShipmentPriceRequest
     */
    public function setReturnDocs(int $returnDocs): ShipmentPriceRequest {
        $this->returnDocs = $returnDocs;

        return $this;
    }

    /**
     * @param boolean $saturdayDelivery
     * @return ShipmentPriceRequest
     */
    public function setSaturdayDelivery(bool $saturdayDelivery): ShipmentPriceRequest {
        $this->saturdayDelivery = $saturdayDelivery;

        return $this;
    }

    /**
     * @param string $city
     * @return ShipmentPriceRequest
     */
    public function setCity(string $city): ShipmentPriceRequest {
        $this->city = $city;

        return $this;
    }

    /**
     * @param string $county
     * @return ShipmentPriceRequest
     */
    public function setCounty(string $county): ShipmentPriceRequest {
        $this->county = $county;

        return $this;
    }

    /**
     * @param boolean $toOffice
     * @return ShipmentPriceRequest
     */
    public function setToOffice(bool $toOffice): ShipmentPriceRequest {
        $this->toOffice = $toOffice;

        return $this;
    }

    /**
     * @param string $currency
     * @return ShipmentPriceRequest
     */
    public function setCurrency(string $currency): ShipmentPriceRequest {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return ShipmentPrice
     */
    public function makeRequest(Eushipments $eushipments): ShipmentPrice {
        $url = $eushipments->getApiUrl() . '/shipment-price';

        $bodyParams = [
            'courierId' => $this->courierId,
            'companyId' => $this->companyId,
            'weight' => $this->weight,
            'codAmount' => $this->codAmount,
            'openPackage' => $this->openPackage,
            'insuranceAmount' => $this->insuranceAmount,
            'returnDocs' => $this->returnDocs,
            'saturdayDelivery' => $this->saturdayDelivery
        ];

        if ($this->city !== null) {
            $bodyParams['city'] = $this->city;
        }

        if ($this->county !== null) {
            $bodyParams['county'] = $this->county;
        }

        if ($this->toOffice !== null) {
            $bodyParams['toOffice'] = $this->toOffice;
        }

        if ($this->currency !== null) {
            $bodyParams['currency'] = $this->currency;
        }

        $rawResponse = $this->sendRequest($url, 'POST', [], $eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        return ShipmentPrice::withJson($response);
    }
}
