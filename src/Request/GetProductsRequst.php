<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\Product;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class GetProductsRequst extends Request {

    /**
     * The ID of the company. Can be obtained by CompaniesRequest.
     *
     * @var integer
     */
    public $companyId;

    /**
     * @param integer $companyId
     * @return GetProductsRequst
     */
    public function setCompanyId(int $companyId): GetProductsRequst {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return Product[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/fulfilment/get-products';

        $bodyParams = [
            'companyId' => $this->companyId
        ];

        $rawResponse = $this->sendRequest($url, 'GET', [], $eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $products = [];
        foreach ($response as $value) {
            $products[] = Product::withJson($value);
        }

        return $products;
    }
}
