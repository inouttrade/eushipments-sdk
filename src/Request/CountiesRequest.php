<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\County;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CountiesRequest extends Request {

    /**
     * Country ID, obtained from `CountriesRequest` or euShipments website
     *
     * @var integer
     */
    public $countryId;

    /**
     * @param integer $countryId
     * @return CountiesRequest
     */
    public function setCountryId(int $countryId): CountiesRequest {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return County[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/get-counties/' . $this->countryId;

        $rawResponse = $this->sendRequest($url, 'GET', [], $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $counties = [];
        foreach ($response as $value) {
            $counties[] = County::withJson($value);
        }

        return $counties;
    }
}
