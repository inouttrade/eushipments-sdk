<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CreateRequestRequest extends Request {

    /**
     *  Warehouses
     */
    const WAREHOUSE_RUSE = 1;

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * The ID of the company. Can be obtained by CompaniesRequest.
     *
     * @var integer
     */
    public $companyId;

    /**
     * ID of warehouse you want to send a Request to
     *
     * @var integer
     */
    public $warehouseId = self::WAREHOUSE_RUSE;

    /**
     * @var array
     */
    public $products;

    /**
     * @param boolean $testMode
     * @return CreateRequestRequest
     */
    public function setTestMode(bool $testMode): CreateRequestRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param integer $companyId
     * @return CreateRequestRequest
     */
    public function setCompanyId(int $companyId): CreateRequestRequest {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * @param integer $warehouseId
     * @return CreateRequestRequest
     */
    public function setWarehouseId(int $warehouseId): CreateRequestRequest {
        $this->warehouseId = $warehouseId;

        return $this;
    }

    /**
     * @param array $products
     * @return CreateRequestRequest
     */
    public function setProducts(array $products): CreateRequestRequest {
        $this->products = $products;

        return $this;
    }

    /**
     * @param Eushipments$eushipments
     * @return string
     */
    public function makeRequest(Eushipments$eushipments): string {
        $url =$eushipments->getApiUrl() . '/fulfilment/create-request';

        $bodyParams = [
            'testMode' => $this->testMode ? 1 : 0,
            'companyId' => $this->companyId,
            'warehouseId' => $this->warehouseId,
            'products' => $this->products
        ];

        $rawResponse = $this->sendRequest($url, 'POST', [],$eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error') && !empty($response->error)) {
            throw new Exception($response->error);
        }

        return $response->requestId;
    }
}
