<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Datastructure\Action;
use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class PhoneCallHistoryRequest extends Request {

    /**
     * Waybill number
     *
     * @var string
     */
    public $awbNumber;

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * @param string $awbNumber
     * @return PhoneCallHistoryRequest
     */
    public function setAwbNumber(string $awbNumber): PhoneCallHistoryRequest {
        $this->awbNumber = $awbNumber;

        return $this;
    }

    /**
     * @param boolean $testMode
     * @return PhoneCallHistoryRequest
     */
    public function setTestMode(bool $testMode): PhoneCallHistoryRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return Action[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/check-actions/' . $this->awbNumber;

        $queryParams = [
            'testMode' => $this->testMode ? 1 : 0
        ];

        $rawResponse = $this->sendRequest($url, 'GET', $queryParams, $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $actions = [];
        foreach ($response->actions as $value) {
            $actions[] = Action::withJson($value);
        }

        return $actions;
    }
}
