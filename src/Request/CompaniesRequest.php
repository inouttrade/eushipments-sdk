<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\Company;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CompaniesRequest extends Request {

    /**
     * (Optional) If you want to use the API only for tests, set this field to `true`. Applies only to the production
     * environment. On the test environment, all the data is saved on test environment.
     *
     * @var boolean|null
     */
    public $testMode = true;

    /**
     * @param boolean $testMode
     * @return CompaniesRequest
     */
    public function setTestMode(bool $testMode): CompaniesRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return Company[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/get-user-companies';

        $queryParams = [];

        if ($this->testMode !== null) {
            $queryParams['testMode'] = $this->testMode ? 1 : 0;
        }

        $rawResponse = $this->sendRequest($url, 'GET', $queryParams, $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $companies = [];
        foreach ($response as $value) {
            $companies[] = Company::withJson($value);
        }

        return $companies;
    }
}
