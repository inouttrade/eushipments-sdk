<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class ShipmentStatusRequest extends Request {

    /**
     * Waybill number
     *
     * @var string
     */
    public $awbNumber;

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * @param string $awbNumber
     * @return ShipmentStatusRequest
     */
    public function setAwbNumber(string $awbNumber): ShipmentStatusRequest {
        $this->awbNumber = $awbNumber;

        return $this;
    }

    /**
     * @param boolean $testMode
     * @return ShipmentStatusRequest
     */
    public function setTestMode(bool $testMode): ShipmentStatusRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return string
     */
    public function makeRequest(Eushipments $eushipments): string {
        $url = $eushipments->getApiUrl() . '/get-status/' . $this->awbNumber;

        $queryParams = [
            'testMode' => $this->testMode ? 1 : 0
        ];

        $rawResponse = $this->sendRequest($url, 'GET', $queryParams, $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        return $response->status;
    }
}
