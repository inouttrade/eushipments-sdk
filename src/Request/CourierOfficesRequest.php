<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\CourierOffice;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CourierOfficesRequest extends Request {

    /**
     * Courier ID, obtained from `CouriersRequest` or euShipments website
     *
     * @var integer
     */
    public $courierId;

    /**
     * @param integer $courierId
     * @return CourierOfficesRequest
     */
    public function setCourierId(int $courierId): CourierOfficesRequest {
        $this->courierId = $courierId;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return CourierOffice[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/offices-by-courier/' . $this->courierId;

        $rawResponse = $this->sendRequest($url, 'GET', [], $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $offices = [];
        foreach ($response as $value) {
            $temp = CourierOffice::withJson($value, 2);

            if ($temp !== null) {
                $offices[] = $temp;
            }
        }

        return $offices;
    }
}
