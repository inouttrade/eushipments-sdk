<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Datastructure\Product;
use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CreateProductRequest extends Request {

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * The ID of the company. Can be obtained by CompaniesRequest.
     *
     * @var integer
     */
    public $companyId;

    /**
     * @var Product
     */
    public $product;

    /**
     * @param boolean $testMode
     * @return CreateProductRequest
     */
    public function setTestMode(bool $testMode): CreateProductRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param integer $companyId
     * @return CreateProductRequest
     */
    public function setCompanyId(int $companyId): CreateProductRequest {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * @param Product $product
     * @return CreateProductRequest
     */
    public function setProduct(Product $product): CreateProductRequest {
        $this->product = $product;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return string
     */
    public function makeRequest(Eushipments $eushipments): string {
        $url = $eushipments->getApiUrl() . '/fulfilment/create-product';

        $bodyParams = [
            'testMode' => $this->testMode ? 1 : 0,
            'companyId' => $this->companyId,
            'product' => $this->product
        ];

        $rawResponse = $this->sendRequest($url, 'POST', [], $eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error') && !empty($response->error)) {
            // TODO: Extend to return both `error` and `productId`
            throw new Exception($response->error);
        }

        return $response->productId;
    }
}
