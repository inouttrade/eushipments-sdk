<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\Courier;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CouriersRequest extends Request {

    /**
     * Company ID, obtained from `CompaniesRequest` or euShipments website
     *
     * @var integer
     */
    public $companyId;

    /**
     * @param integer $companyId
     * @return CouriersRequest
     */
    public function setCompanyId(int $companyId): CouriersRequest {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return Courier[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/couriers/' . $this->companyId;

        $rawResponse = $this->sendRequest($url, 'GET', [], $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $couriers = [];
        foreach ($response as $value) {
            $couriers[] = Courier::withJson($value);
        }

        return $couriers;
    }
}
