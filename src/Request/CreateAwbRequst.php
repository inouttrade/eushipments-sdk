<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Datastructure\Airwaybill;
use BogdanKovachev\Eushipments\Datastructure\Recipient;
use BogdanKovachev\Eushipments\Datastructure\Sender;
use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CreateAwbRequst extends Request {

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * Company ID, obtained from `CompaniesRequest` or euShipments website
     *
     * @var integer
     */
    public $senderId;

    /**
     * (Optional) Courier ID, obtained from `CouriersRequest` or euShipments website
     *
     * @var integer
     */
    public $courierId;

    /**
     * The date on which the shipment is to be dispatched
     *
     * @var string
     */
    public $waybillAvailableDate;

    /**
     * (Optional) The service name. See all available options in `Service`.
     *
     * @var string
     */
    public $serviceName;

    /**
     * @var Recipient
     */
    public $recipient;

    /**
     * @var Airwaybill
     */
    public $awb;

    /**
     * (Optional) Sender information
     *
     * @var Sender|null
     */
    public $sender;

    /**
     * @var array
     */
    public $document;

    /**
     * @var array
     */
    public $customsData;

    /**
     * @var array
     */
    public $returnLabel;

    /**
     * @var array
     */
    public $courierRequest;

    /**
     * @param boolean $testMode
     * @return CreateAwbRequst
     */
    public function setTestMode(bool $testMode): CreateAwbRequst {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param integer $senderId
     * @return CreateAwbRequst
     */
    public function setSenderId(int $senderId): CreateAwbRequst {
        $this->senderId = $senderId;

        return $this;
    }

    /**
     * @param integer $courierId
     * @return CreateAwbRequst
     */
    public function setCourierId(int $courierId): CreateAwbRequst {
        $this->courierId = $courierId;

        return $this;
    }

    /**
     * @param string $waybillAvailableDate
     * @return CreateAwbRequst
     */
    public function setWaybillAvailableDate(string $waybillAvailableDate): CreateAwbRequst {
        $this->waybillAvailableDate = $waybillAvailableDate;

        return $this;
    }

    /**
     * @param string $serviceName
     * @return CreateAwbRequst
     */
    public function setServiceName(string $serviceName): CreateAwbRequst {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * @param Recipient $recipient
     * @return CreateAwbRequst
     */
    public function setRecipient(Recipient $recipient): CreateAwbRequst {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * @param Airwaybill $awb
     * @return CreateAwbRequst
     */
    public function setAwb(Airwaybill $awb): CreateAwbRequst {
        $this->awb = $awb;

        return $this;
    }

    /**
     * @param Sender $sender
     * @return CreateAwbRequst
     */
    public function setSender(Sender $sender): CreateAwbRequst {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @param array $document
     * @return CreateAwbRequst
     */
    public function setDocument(array $document): CreateAwbRequst {
        $this->document = $document;

        return $this;
    }

    /**
     * @param array $customsData
     * @return CreateAwbRequst
     */
    public function setCustomsData(array $customsData): CreateAwbRequst {
        $this->customsData = $customsData;

        return $this;
    }

    /**
     * @param array $returnLabel
     * @return CreateAwbRequst
     */
    public function setReturnLabel(array $returnLabel): CreateAwbRequst {
        $this->returnLabel = $returnLabel;

        return $this;
    }

    /**
     * @param array $courierRequest
     * @return CreateAwbRequst
     */
    public function setCourierRequest(array $courierRequest): CreateAwbRequst {
        $this->courierRequest = $courierRequest;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return string
     */
    public function makeRequest(Eushipments $eushipments): string {
        $url = $eushipments->getApiUrl() . '/createAWB';

        $bodyParams = [
            'testMode' => $this->testMode ? 1 : 0,
            'senderId' => $this->senderId,
            'waybillAvailableDate' => $this->waybillAvailableDate,
            'recipient' => $this->recipient,
            'awb' => $this->awb
        ];

        if ($this->courierId !== null) {
            $bodyParams['courierId'] = $this->courierId;
        }

        if ($this->serviceName !== null) {
            $bodyParams['serviceName'] = $this->serviceName;
        }

        if ($this->sender !== null) {
            $bodyParams['sender'] = $this->sender;
        }

        if ($this->document !== null) {
            $bodyParams['document'] = $this->document;
        }

        if ($this->customsData !== null) {
            $bodyParams['customsData'] = $this->customsData;
        }

        if ($this->returnLabel !== null) {
            $bodyParams['returnLabel'] = $this->returnLabel;
        }

        if ($this->courierRequest !== null) {
            $bodyParams['courierRequest'] = $this->courierRequest;
        }

        $rawResponse = $this->sendRequest($url, 'POST', [], $eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        return $response->awb;
    }
}
