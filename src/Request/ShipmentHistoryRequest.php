<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\StatusHistory;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class ShipmentHistoryRequest extends Request {

    /**
     * Waybill number
     *
     * @var string
     */
    public $awbNumber;

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * Prefered language of comments
     *
     * @var string
     */
    public $language = 'EN';

    /**
     * @param string $awbNumber
     * @return ShipmentHistoryRequest
     */
    public function setAwbNumber(string $awbNumber): ShipmentHistoryRequest {
        $this->awbNumber = $awbNumber;

        return $this;
    }

    /**
     * @param boolean $testMode
     * @return ShipmentHistoryRequest
     */
    public function setTestMode(bool $testMode): ShipmentHistoryRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param string $language
     * @return ShipmentHistoryRequest
     */
    public function setLanguage(string $language): ShipmentHistoryRequest {
        $this->language = $language;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return StatusHistory[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/get-status/history/' . $this->awbNumber;

        $queryParams = [
            'testMode' => $this->testMode ? 1 : 0,
            'lang' => $this->language
        ];

        $rawResponse = $this->sendRequest($url, 'GET', $queryParams, $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $history = [];
        foreach ($response->statusesHistory as $value) {
            $history[] = StatusHistory::withJson($value);
        }

        return $history;
    }
}
