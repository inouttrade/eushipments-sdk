<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\City;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CitySuggestionsRequest extends Request {

    /**
     * Country ID, obtained from `CountriesRequest` or euShipments website
     *
     * @var integer
     */
    public $countryId;

    /**
     * Keyword (minimum 3 characters) used in search
     *
     * @var string
     */
    public $keyword;

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * By default, the system searches the city by ZIP code or city name. If you want to search by all fields you should
     * set this field to `true`.
     *
     * @var boolean|null
     */
    public $searchAllFields;

    /**
     * @param integer $countryId
     * @return CitySuggestionsRequest
     */
    public function setCountryId(int $countryId): CitySuggestionsRequest {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * @param string $keyword
     * @return CitySuggestionsRequest
     */
    public function setKeyword(string $keyword): CitySuggestionsRequest {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * @param boolean $testMode
     * @return CitySuggestionsRequest
     */
    public function setTestMode(bool $testMode): CitySuggestionsRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param boolean $searchAllFields
     * @return CitySuggestionsRequest
     */
    public function setSearchAllFields(bool $searchAllFields): CitySuggestionsRequest {
        $this->searchAllFields = $searchAllFields;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return City[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/get-cities/suggestions/' . $this->countryId . '/' . rawurlencode($this->keyword);

        $queryParams = [
            'testMode' => $this->testMode ? 1 : 0
        ];

        if ($this->searchAllFields !== null) {
            $queryParams['searchAllFields'] = $this->searchAllFields ? 1 : 0;
        }

        $rawResponse = $this->sendRequest($url, 'GET', $queryParams, $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        if (is_object($response) && property_exists($response, 'errors')) {
            $temp = reset($response->errors);

            throw new Exception(array_shift($temp));
        }

        $cities = [];
        foreach ($response->suggestions as $value) {
            $cities[] = City::withJson($value, 1);
        }

        return $cities;
    }
}
