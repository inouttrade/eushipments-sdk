<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Datastructure\Order;
use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class OrdersHistoryRequest extends Request {

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * @var array
     */
    public $orders;

    /**
     * @param boolean $testMode
     * @return OrdersHistoryRequest
     */
    public function setTestMode(bool $testMode): OrdersHistoryRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param array $orders
     * @return OrdersHistoryRequest
     */
    public function setOrders(array $orders): OrdersHistoryRequest {
        $this->orders = $orders;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return Order[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/fulfilment/orders-history';

        $temp = [];
        foreach ($this->orders as $order) {
            $temp[] = [
                'refNum' => $order
            ];
        }

        $bodyParams = [
            'testMode' => $this->testMode ? 1 : 0,
            'orders' => $temp
        ];

        $rawResponse = $this->sendRequest($url, 'POST', [], $eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error') && !empty($response->error)) {
            throw new Exception($response->error);
        }

        $temp = [];
        foreach ($response as $value) {
            $temp[] = Order::withJson($value);
        }

        return $temp;
    }
}
