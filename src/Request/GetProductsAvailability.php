<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Datastructure\ProductAvailability;
use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class GetProductsAvailability extends Request {

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * The ID of the company. Can be obtained by CompaniesRequest.
     *
     * @var integer
     */
    public $companyId;

    /**
     * @param boolean $testMode
     * @return GetProductsAvailability
     */
    public function setTestMode(bool $testMode): GetProductsAvailability {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param integer $companyId
     * @return GetProductsAvailability
     */
    public function setCompanyId(int $companyId): GetProductsAvailability {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return ProductAvailability[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/fulfilment/get-prod-avails';

        $bodyParams = [
            'testMode' => $this->testMode ? 1 : 0,
            'companyId' => $this->companyId
        ];

        $rawResponse = $this->sendRequest($url, 'GET', [], $eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $products = [];
        foreach ($response as $value) {
            $products[] = ProductAvailability::withJson($value);
        }

        return $products;
    }
}
