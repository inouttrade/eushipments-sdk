<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Datastructure\Document;
use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class PrintRequest extends Request {

    /**
     * AWB number
     *
     * @var string
     */
    public $awbNumber;

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * (Optional) By default always returns label. But you can choose the type of AWB print. If 'printFileType' equals
     * to 1 then you receive PDF in A4 format, else if 'printFileType' equals to 2 then you receive PDF in label format.
     * This works if our partner also supports a waybill in both formats.
     * Important: If we set the type label in our system you receive the PDF in the chosen format in out system.
     *
     * @var integer
     */
    public $printFileType = 1;

    /**
     * @param string $awbNumber
     * @return PrintRequest
     */
    public function setAwbNumber(string $awbNumber): PrintRequest {
        $this->awbNumber = $awbNumber;

        return $this;
    }

    /**
     * @param boolean $testMode
     * @return PrintRequest
     */
    public function setTestMode(bool $testMode): PrintRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param integer $printFileType
     * @return PrintRequest
     */
    public function setPrintFileType(int $printFileType): PrintRequest {
        $this->printFileType = $printFileType;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return Document
     */
    public function makeRequest(Eushipments $eushipments): Document {
        $url = $eushipments->getApiUrl() . '/print/' . $this->awbNumber;

        $queryParams = [
            'testMode' => $this->testMode ? 1 : 0,
            'printFileType' => $this->printFileType
        ];

        $rawResponse = $this->sendRequest($url, 'GET', $queryParams, $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        return Document::withJson($response);
    }
}
