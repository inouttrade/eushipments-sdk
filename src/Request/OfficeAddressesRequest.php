<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\CourierOffice;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class OfficeAddressesRequest extends Request {

    /**
     * City ID, obtained from `CitiesRequest`
     *
     * @var integer
     */
    public $cityId;

    /**
     * @param integer $cityId
     * @return OfficeAddressesRequest
     */
    public function setCityId(int $cityId): OfficeAddressesRequest {
        $this->cityId = $cityId;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return CourierOffice[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/office-addresses';

        $bodyParams = [
            'cityId' => $this->cityId
        ];

        $rawResponse = $this->sendRequest($url, 'GET', [], $eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        $offices = [];
        foreach ($response->offices as $value) {
            $offices[] = CourierOffice::withJson($value, 1);
        }

        return $offices;
    }
}
