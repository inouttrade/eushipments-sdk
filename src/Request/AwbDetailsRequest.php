<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Datastructure\Airwaybill;
use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class AwbDetailsRequest extends Request {

    /**
     * AWB number
     *
     * @var string
     */
    public $awbNumber;

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * @param string $awbNumber
     * @return AwbDetailsRequest
     */
    public function setAwbNumber(string $awbNumber): AwbDetailsRequest {
        $this->awbNumber = $awbNumber;

        return $this;
    }

    /**
     * @param boolean $testMode
     * @return AwbDetailsRequest
     */
    public function setTestMode(bool $testMode): AwbDetailsRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return Airwaybill
     */
    public function makeRequest(Eushipments $eushipments): Airwaybill {
        $url = $eushipments->getApiUrl() . '/get-awb/' . $this->awbNumber;

        $queryParams = [
            'testMode' => $this->testMode ? 1 : 0
        ];

        $rawResponse = $this->sendRequest($url, 'GET', $queryParams, $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        return Airwaybill::withJson($response);
    }
}
