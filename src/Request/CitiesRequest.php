<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\City;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CitiesRequest extends Request {

    /**
     * Country ID, obtained from CountriesRequest or euShipments website
     *
     * @var integer
     */
    public $countryId;

    /**
     * Enable pagination for records returned. When it's enabled only few records are returned, otherwise - all records.
     *
     * @var boolean
     */
    public $paging;

    /**
     * (Conditional) Number of records to be returned. Required when the pagination is enabled.
     *
     * @var integer|null
     */
    public $first;

    /**
     * (Conditional) Number of records to be skiped. Required when the pagination is enabled.
     *
     * @var integer|null
     */
    public $skip;

    /**
     * (Optional) County ID, obtained from `CountiesRequest`. You should use this property only for Romania.
     *
     * @var integer|null
     */
    public $countyId;

    /**
     * (Optional) Office ID, obtained from `RomanianOfficesRequest`. You should use this property only for Romania.
     *
     * @var integer|null
     */
    public $officeId;

    /**
     * @param integer $countryId
     * @return CitiesRequest
     */
    public function setCountryId(int $countryId): CitiesRequest {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * @param boolean $paging
     * @return CitiesRequest
     */
    public function setPaging(bool $paging): CitiesRequest {
        $this->paging = $paging;

        return $this;
    }

    /**
     * @param integer $first
     * @return CitiesRequest
     */
    public function setFirst(int $first): CitiesRequest {
        $this->first = $first;

        return $this;
    }

    /**
     * @param integer $skip
     * @return CitiesRequest
     */
    public function setSkip(int $skip): CitiesRequest {
        $this->skip = $skip;

        return $this;
    }

    /**
     * @param integer $countyId
     * @return CitiesRequest
     */
    public function setCountyId(int $countyId): CitiesRequest {
        $this->countyId = $countyId;

        return $this;
    }

    /**
     * @param integer $officeId
     * @return CitiesRequest
     */
    public function setOfficeId(int $officeId): CitiesRequest {
        $this->officeId = $officeId;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return City[]
     */
    public function makeRequest(Eushipments $eushipments): array {
        $url = $eushipments->getApiUrl() . '/get-cities/' . $this->countryId;

        $queryParams = [
            'paging' => $this->paging ? 1 : 0
        ];

        if ($this->first !== null) {
            $queryParams['first'] = $this->first;
        }

        if ($this->skip !== null) {
            $queryParams['skip'] = $this->skip;
        }

        if ($this->countryId !== null) {
            $queryParams['country_id'] = $this->countryId;
        }

        if ($this->officeId !== null) {
            $queryParams['office_id'] = $this->officeId;
        }

        $rawResponse = $this->sendRequest($url, 'GET', $queryParams, $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        if (is_object($response) && property_exists($response, 'errors')) {
            $temp = reset($response->errors);

            throw new Exception(array_shift($temp));
        }

        $cities = [];
        foreach ($response as $value) {
            $cities[] = City::withJson($value, 2);
        }

        return $cities;
    }
}
