<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use BogdanKovachev\Eushipments\Datastructure\City;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class CityRequest extends Request {

    /**
     * Country ID, obtained from `CountriesRequest` or euShipments website
     *
     * @var integer
     */
    public $countryId;

    /**
     * Postcode (ZIP)
     *
     * @var string
     */
    public $zipCode;

    /**
     * City name
     *
     * @var string
     */
    public $cityName;

    /**
     * @param integer $countryId
     * @return CityRequest
     */
    public function setCountryId(int $countryId): CityRequest {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * @param string $zipCode
     * @return CityRequest
     */
    public function setZipCode(string $zipCode): CityRequest {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @param string $cityName
     * @return CityRequest
     */
    public function setCityName(string $cityName): CityRequest {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return City
     */
    public function makeRequest(Eushipments $eushipments): City {
        $url = $eushipments->getApiUrl() . '/get-city/by/' . $this->countryId . '/' . rawurlencode($this->zipCode) . '/' . rawurlencode($this->cityName);

        $rawResponse = $this->sendRequest($url, 'GET', [], $eushipments->authToken);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error')) {
            throw new Exception($response->error);
        }

        return City::withJson($response, 3);
    }
}
