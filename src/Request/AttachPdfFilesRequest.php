<?php

namespace BogdanKovachev\Eushipments\Request;

use BogdanKovachev\Eushipments\Eushipments;
use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class AttachPdfFilesRequest extends Request {

    /**
     * @var boolean
     */
    public $testMode = true;

    /**
     * (Optional) ID of order. Required if the reference number not present.
     * If both `referenceNumber` and `orderId` are sent, `orderId` takes precedence and `referenceNumber` is ignored.
     *
     * @var integer|null
     */
    public $orderId;

    /**
     * (Optional) Reference number (usually the reference number is the order number from your system.)
     * If both `referenceNumber` and `orderId` are sent, `orderId` takes precedence and `referenceNumber` is ignored.
     *
     * @var string|null
     */
    public $referenceNumber;

    /**
     * Base64 encoded files
     *
     * @var array
     */
    public $files;

    /**
     * @param boolean $testMode
     * @return AttachPdfFilesRequest
     */
    public function setTestMode(bool $testMode): AttachPdfFilesRequest {
        $this->testMode = $testMode;

        return $this;
    }

    /**
     * @param integer $orderId
     * @return AttachPdfFilesRequest
     */
    public function setOrderId(int $orderId): AttachPdfFilesRequest {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * @param string $referenceNumber
     * @return AttachPdfFilesRequest
     */
    public function setReferenceNumber(string $referenceNumber): AttachPdfFilesRequest {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * @param array $files
     * @return AttachPdfFilesRequest
     */
    public function setFiles(array $files): AttachPdfFilesRequest {
        $this->files = $files;

        return $this;
    }

    /**
     * @param Eushipments $eushipments
     * @return integer
     */
    public function makeRequest(Eushipments $eushipments): int {
        $url = $eushipments->getApiUrl() . '/fulfilment/attach-pdf-files';

        $bodyParams = [
            'testMode' => $this->testMode ? 1 : 0,
            'files' => $this->files
        ];

        if ($this->orderId !== null) {
            $bodyParams['orderId'] = $this->orderId;
        }

        if ($this->referenceNumber !== null) {
            $bodyParams['referenceNumber'] = $this->referenceNumber;
        }

        $rawResponse = $this->sendRequest($url, 'POST', [], $eushipments->authToken, $bodyParams);

        $response = json_decode($rawResponse);

        if (is_object($response) && property_exists($response, 'error') && !empty($response->error)) {
            throw new Exception($response->error);
        }

        $orderIds = [];
        foreach ($response as $obj) {
            if (is_object($obj) && property_exists($obj, 'error') && !empty($obj->error)) {
                throw new Exception($obj->error);
            }

            $orderIds[] = $obj->orderId;
        }

        return $orderIds[0];
    }
}
