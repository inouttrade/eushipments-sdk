<?php

namespace BogdanKovachev\Eushipments;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class ShipmentStatus {

    const ON_DELIVERY = 'On delivery';
    const IN_TRANSIT = 'In transit';
    const DELIVERED = 'Delivered';
    const RETURNED = 'Returned';
    const IN_THE_OFFICE = 'In the office';
    const COD_PAID = 'COD paid';
    const NEWNEW = 'New';
    const CANCELLED = 'Cancelled';
    const STOCKOUT = 'Stockout';
    const LOST_SHIPMENT = 'Lost shipment';
    const REDIRECTED = 'Redirected';
    const DELETED = 'Deleted';
    const WAREHOUSE_RUSE = 'Warehouse Ruse';
    const RETURNING = 'Returning';
}
