<?php

namespace BogdanKovachev\Eushipments;

use Exception;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class FileEncoder {

    /**
     * Return base64 encoded content of the file (local or remote)
     *
     * @param string $filePath
     * @return string
     */
    public static function encode(string $filePath): string {
        $file = @file_get_contents($filePath);
        if ($file === false) {
            throw new Exception('The provided file cannot be read.');
        }

        return base64_encode($file);
    }

    /**
     * Return Base64 decoded string
     *
     * @param string $data
     * @return string
     */
    public static function decode(string $data): string {
        $temp = base64_decode($data, true);
        if ($temp === false) {
            throw new Exception('The provided data is corrupted.');
        }

        return $temp;
    }
}
