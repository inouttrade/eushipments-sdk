<?php

namespace BogdanKovachev\Eushipments;

/**
 * @author Bogdan Kovachev (https://1337.bg)
 */
class ReturnDocs {

    const DOCUMENT = 0;
    const WAYBILL = 1;
    const NOTHING = 2;
}
